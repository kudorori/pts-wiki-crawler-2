import React from 'react';
import {CardFooter, FormGroup, Input} from 'reactstrap';

type PanelFooterProps = {
  itemsPerPage: number;
  itemCount: number;
  currentPage: number;
  updatePanel?: Function;
  setCurrentPage?: Function;
};

const defaultProps = {
  itemsPerPage: 10,
  itemCount: 0,
  currentPage: 1,
};

const PanelFooter = ({
  itemsPerPage,
  itemCount,
  currentPage,
  setCurrentPage,
}: PanelFooterProps) => {
  const goToPage = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setCurrentPage(event.target.value);
  };

  const pageCountTotal = Math.ceil(itemCount / itemsPerPage);

  const optionElements = [...Array(pageCountTotal).keys()].map(index => (
    <option key={index} value={index + 1} data-testid="select-option">
      第 {index + 1} 頁
    </option>
  ));

  return (
    <CardFooter className="bg-primary">
      <FormGroup className="d-flex justify-content-end align-items-center mb-0">
        <span className="mr-3 text-light" data-testid="page-desc">
          共 <span>{itemCount}</span> 筆，目前位於 {pageCountTotal} 頁中的
        </span>
        <Input
          data-testid="select"
          className="w-auto"
          type="select"
          value={currentPage}
          onChange={goToPage}
        >
          {optionElements}
        </Input>
      </FormGroup>
    </CardFooter>
  );
};

PanelFooter.defaultProps = defaultProps;

export default PanelFooter;
