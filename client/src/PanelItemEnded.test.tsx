import {waitFor, render, screen} from '@testing-library/react';
import PanelItemEnded from './PanelItemEnded';

it('should properly rendered without props', async () => {
  render(<PanelItemEnded />);
  await waitFor(() => {
    expect(screen.getByTestId('search-url')).toBeInTheDocument();
    expect(screen.getByTestId('search-url')).toHaveTextContent(
      'https://zh.wikipedia.org'
    );
    expect(screen.getByTestId('index')).toHaveTextContent('0');
  });
});

it('should properly rendered when type="WikiCrawler"', async () => {
  const task = {
    index: 1,
    id: 0,
    createdAt: '1900/01/01 00:00:00',
    doneAt: '2020/12/31 23:59:59',
    searchUrl: 'https://www.google.com',
    depthNum: 1,
    fileName: 'upload.json',
    faceBoxUrl: 'https://facebox.com/01',
    tagBoxUrl: 'https://tagbox.com/01',
    failedMessage: '',
    progressCount: 0,
    progressTotal: 100,
    status: 'FINISHED',
  };
  render(<PanelItemEnded task={task} />);
  await waitFor(() => {
    expect(screen.getByTestId('search-url')).toBeInTheDocument();
    expect(screen.getByTestId('search-url')).toHaveTextContent(
      'https://www.google.com'
    );
    expect(screen.getByTestId('index')).toHaveTextContent('1');
    expect(screen.getByTestId('download-link')).toHaveAttribute(
      'href',
      '/api/wiki_crawler/list/0/download'
    );
  });
});

it('should properly rendered when type="Teaching"', async () => {
  const task = {
    index: 1,
    id: 0,
    createdAt: '1900/01/01 00:00:00',
    doneAt: '2020/12/31 23:59:59',
    searchUrl: 'https://www.google.com',
    depthNum: 1,
    fileName: 'upload.json',
    faceBoxUrl: 'https://facebox.com/01',
    tagBoxUrl: 'https://tagbox.com/01',
    failedMessage: '',
    progressCount: 0,
    progressTotal: 100,
    status: 'FINISHED',
  };
  render(<PanelItemEnded task={task} type="Teaching" />);
  await waitFor(() => {
    expect(screen.getByTestId('file-name')).toBeInTheDocument();
    expect(screen.getByTestId('file-name')).toHaveTextContent('upload.json');
  });
});
