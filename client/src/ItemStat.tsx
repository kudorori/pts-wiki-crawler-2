import React from 'react';
import {ListGroupItem, Spinner} from 'reactstrap';

type ItemStatProps = {
  status: 'pending' | 'empty' | 'error';
};

const defaultProps = {
  status: 'pending',
};

const ItemStat = ({status}: ItemStatProps) => {
  const pendingElement = (
    <Spinner type="grow" color="secondary" data-testid="spinner" />
  );
  const emptyElement = <span className="text-secondary">目前沒有工作</span>;
  const errerElement = <span className="text-danger">發生錯誤</span>;
  const statusMap = new Map([
    ['pending', pendingElement],
    ['empty', emptyElement],
    ['error', errerElement],
  ]);
  return (
    <ListGroupItem>
      <div className="text-center p-3">{statusMap.get(status)}</div>
    </ListGroupItem>
  );
};

ItemStat.defaultProps = defaultProps;

export default ItemStat;
