import {render, screen, waitFor, fireEvent} from '@testing-library/react';
import App from './App';

it('should properly rendered when routing to /', async () => {
  render(<App />);
  expect(screen.getByText(/維基爬蟲/)).toBeInTheDocument();
});

it('should properly rendered when routing to /teaching', async () => {
  render(<App />);
  await waitFor(
    async () => {
      fireEvent.click(screen.getByText(/Machine Box 匯入/));
      await waitFor(
        async () => {
          await waitFor(() => {
            expect(screen.getByText(/FaceBox 網址：/)).toBeInTheDocument();
          });
        },
        {timeout: 5000}
      );
    },
    {timeout: 5000}
  );
});
