import React, {useState, useEffect, useRef} from 'react';
import axios from 'axios';
import {checkValidUrl, checkValidFile, getErrorMsg} from './utils';
import {Card, CardBody, Button, Label, FormGroup, Input} from 'reactstrap';
import PanelHeader from './PanelHeader';

type TeachingCreateProps = {
  createApi: string;
  setNeedUpdatePanel: Function;
};

const TeachingCreate = ({
  createApi,
  setNeedUpdatePanel,
}: TeachingCreateProps) => {
  const [faceBoxUrl, setFaceBoxUrl] = useState('');
  const [tagBoxUrl, setTagBoxUrl] = useState('');
  const [file, setfile] = useState(null);
  const [isValidFaceBoxUrl, checkFaceBoxUrl] = useState(false);
  const [isValidTagBoxUrl, checkTagBoxUrl] = useState(false);
  const [isValidFile, checkFile] = useState(false);
  const [errorMsg, setErrorMsg] = useState('');
  const [isUploading, setIsUploading] = useState(false);
  const submitForm = useRef(null);

  const updateFaceBoxUrl = (event: React.ChangeEvent<HTMLInputElement>) => {
    setFaceBoxUrl(event.target.value);
  };

  const updateTagBoxUrl = (event: React.ChangeEvent<HTMLInputElement>) => {
    setTagBoxUrl(event.target.value);
  };

  const validateFile = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.files.length !== 0) {
      setfile(event.target.files[0]);
    }
  };

  const resetPanel = (): void => {
    setFaceBoxUrl('');
    setTagBoxUrl('');
    setfile(null);
    submitForm.current.reset();
  };

  const createTask = (event: React.FormEvent<HTMLFormElement>): void => {
    setIsUploading(true);
    event.preventDefault();
    const data = new FormData();
    data.append('file', file);
    data.append('faceBoxUrl', faceBoxUrl);
    data.append('tagBoxUrl', tagBoxUrl);
    axios
      .post(createApi, data)
      .then(() => {
        setErrorMsg('');
      })
      .catch(error => {
        setErrorMsg(getErrorMsg(error));
      })
      .finally(() => {
        setNeedUpdatePanel(Date.now());
        setIsUploading(false);
        resetPanel();
      });
  };

  useEffect(() => {
    checkFaceBoxUrl(checkValidUrl(faceBoxUrl));
    checkTagBoxUrl(checkValidUrl(tagBoxUrl));
    if (file) {
      checkFile(checkValidFile(file.name));
    }
  }, [faceBoxUrl, tagBoxUrl, file]);

  const isInvalidFaceBoxUrl = !isValidFaceBoxUrl && faceBoxUrl !== '';
  const isInvalidTagBoxUrl = !isValidTagBoxUrl && tagBoxUrl !== '';
  const isValidFileClass = isValidFile ? 'is-valid' : 'is-invalid';
  const allowSubmit =
    isValidFaceBoxUrl &&
    faceBoxUrl !== '' &&
    isValidTagBoxUrl &&
    tagBoxUrl !== '' &&
    isValidFile &&
    file !== null;
  const allowReset = faceBoxUrl !== '' || tagBoxUrl !== '' || file !== null;
  const btnColor = allowSubmit && !isUploading ? 'success' : 'secondary';
  const btnIconClass = isUploading
    ? 'fas fa-circle-notch fa-spin mr-2'
    : 'fas fa-plus mr-2';
  return (
    <Card className="pwc-status-panel-create mb-3 border-primary">
      <PanelHeader title="加入新工作" errorMsg={errorMsg} />
      <CardBody>
        <form ref={submitForm} onSubmit={createTask} data-testid="create-form">
          <div
            className="d-inline-flex align-items-center w-100 pb-3"
            style={{height: 130}}
          >
            <div className="row d-inline-flex flex-column align-items-center flex-grow-1 mr-1 h-100">
              <div
                className="col d-inline-flex align-items-center"
                style={{marginBottom: '2rem'}}
              >
                <Label
                  for="facebox-url"
                  className="m-0 text-right"
                  style={{width: '20%'}}
                >
                  FaceBox 網址：
                </Label>
                <div className="flex-grow-1">
                  <Input
                    id="facebox-url"
                    className="w-100"
                    placeholder="請輸入 FaceBox 網址"
                    type="url"
                    valid={isValidFaceBoxUrl}
                    invalid={isInvalidFaceBoxUrl}
                    onChange={updateFaceBoxUrl}
                    data-testid="facebox-url"
                  />
                  <div
                    className="position-absolute valid-feedback"
                    data-testid="facebox-url-valid"
                  >
                    網址格式正確
                  </div>
                  <div
                    className="position-absolute invalid-feedback"
                    data-testid="facebox-url-invalid"
                  >
                    網址格式不正確
                  </div>
                </div>
              </div>
              <div className="col d-inline-flex align-items-center">
                <Label
                  for="tagbox-url"
                  className="m-0 text-right"
                  style={{width: '20%'}}
                >
                  TagBox 網址：
                </Label>
                <div className="flex-grow-1">
                  <Input
                    id="tagbox-url"
                    className="w-100"
                    placeholder="請輸入 TabBox 網址"
                    type="url"
                    valid={isValidTagBoxUrl}
                    invalid={isInvalidTagBoxUrl}
                    onChange={updateTagBoxUrl}
                    data-testid="tagbox-url"
                  />
                  <div
                    className="position-absolute valid-feedback"
                    data-testid="tagbox-url-valid"
                  >
                    網址格式正確
                  </div>
                  <div
                    className="position-absolute invalid-feedback"
                    data-testid="tagbox-url-invalid"
                  >
                    網址格式不正確
                  </div>
                </div>
              </div>
            </div>
            <div className="row d-inline-flex flex-column align-items-center flex-grow-0 h-100">
              <div
                className="col d-inline-flex align-items-center"
                style={{marginBottom: '2rem'}}
              >
                <Label for="file" className="m-0">
                  檔案：
                </Label>
                <div className="d-inline mr-3">
                  <FormGroup className="mb-0">
                    <div className="custom-file">
                      <input
                        id="file"
                        className={`custom-file-input ${
                          file !== null && isValidFileClass
                        }`}
                        type="file"
                        style={{paddingRight: 'calc(1.5em + 0.75rem)'}}
                        role="button"
                        accept="application/JSON"
                        onChange={validateFile}
                        data-testid="file-name"
                      />
                      <label className="custom-file-label" htmlFor="file">
                        {file ? file.name : '請選擇上傳檔案'}
                      </label>
                      <div className="position-absolute valid-feedback">
                        合法的檔案類型
                      </div>
                      <div className="position-absolute invalid-feedback">
                        不合法的檔案類型
                      </div>
                    </div>
                  </FormGroup>
                </div>
              </div>
              <div className="col text-right">
                <Button
                  color={'danger'}
                  disabled={!allowReset || isUploading}
                  className="mr-3"
                  onClick={resetPanel}
                  data-testid="btn-reset"
                >
                  <i className="fas fa-broom mr-2"></i>
                  清除
                </Button>
                <Button
                  color={btnColor}
                  disabled={!allowSubmit || isUploading}
                  className="mr-3 px-4"
                  type="submit"
                  data-testid="btn-submit"
                >
                  <i className={btnIconClass} data-testid="fa-spin"></i>
                  新增工作
                </Button>
              </div>
            </div>
          </div>
        </form>
      </CardBody>
    </Card>
  );
};

export default TeachingCreate;
