import {render, screen} from '@testing-library/react';
import Teaching from './Teaching';

it('should properly rendered without props', async () => {
  render(<Teaching />);
  expect(screen.getByText(/進行中工作列表/)).toBeInTheDocument();
});
