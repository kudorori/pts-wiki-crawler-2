import {render, screen} from '@testing-library/react';
import ItemStat from './ItemStat';

it('should properly rendered without props', () => {
  render(<ItemStat />);
  expect(screen.getByTestId('spinner')).toBeInTheDocument();
});

it('should properly rendered when status="pending"', () => {
  render(<ItemStat status="pending" />);
  expect(screen.getByTestId('spinner')).toBeInTheDocument();
});

it('should properly rendered when status="empty"', () => {
  render(<ItemStat status="empty" />);
  expect(screen.getByText('目前沒有工作')).toBeInTheDocument();
});

it('should properly rendered when status="error"', () => {
  render(<ItemStat status="error" />);
  expect(screen.getByText('發生錯誤')).toBeInTheDocument();
});
