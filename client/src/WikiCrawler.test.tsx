import {render, screen} from '@testing-library/react';
import WikiCrawler from './WikiCrawler';

it('should properly rendered without props', async () => {
  render(<WikiCrawler />);
  expect(screen.getByText(/進行中工作列表/)).toBeInTheDocument();
});
