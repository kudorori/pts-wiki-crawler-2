import {render, screen, waitFor, fireEvent} from '@testing-library/react';
import TeachingCreate from './TeachingCreate';

it('should properly rendered without props', async () => {
  const setNeedUpdatePanel = jest.fn();
  render(
    <TeachingCreate
      createApi="/api/teaching"
      setNeedUpdatePanel={setNeedUpdatePanel}
    />
  );
  waitFor(() => {
    expect(screen.getByText(/加入新工作/)).toBeInTheDocument();
  });
});

it('should not have validation class when inputs are empty', async () => {
  const setNeedUpdatePanel = jest.fn();
  render(
    <TeachingCreate
      createApi="/api/teaching"
      setNeedUpdatePanel={setNeedUpdatePanel}
    />
  );
  waitFor(() => {
    expect(screen.getByTestId('facebox-url')).not.toHaveClass('is-invalid');
    expect(screen.getByTestId('facebox-url')).not.toHaveClass('is-valid');
    expect(screen.getByTestId('facebox-url')).toHaveValue('');
    expect(screen.getByTestId('tagbox-url')).not.toHaveClass('is-invalid');
    expect(screen.getByTestId('tagbox-url')).not.toHaveClass('is-valid');
    expect(screen.getByTestId('tagbox-url')).toHaveValue('');
    expect(screen.getByTestId('btn-submit')).toHaveClass('disabled');
    expect(screen.getByTestId('fa-spin')).toHaveClass('fa-plus');
    expect(screen.getByTestId('btn-reset')).not.toHaveClass('disabled');
  });
});

it('should show green valid hint when faceboxURL="https://www.google.com"', async () => {
  const setNeedUpdatePanel = jest.fn();
  render(
    <TeachingCreate
      createApi="/api/teaching"
      setNeedUpdatePanel={setNeedUpdatePanel}
    />
  );
  fireEvent.input(screen.getByTestId('facebox-url'), {
    target: 'https://www.google.com',
  });
  waitFor(() => {
    expect(screen.getByTestId('facebox-url')).toHaveClass('is-valid');
  });
});

it('should show green valid hint when tagboxURL="https://www.google.com"', async () => {
  const setNeedUpdatePanel = jest.fn();
  render(
    <TeachingCreate
      createApi="/api/teaching"
      setNeedUpdatePanel={setNeedUpdatePanel}
    />
  );
  fireEvent.input(screen.getByTestId('tagbox-url'), {
    target: 'https://www.google.com',
  });
  waitFor(() => {
    expect(screen.getByTestId('tagbox-url')).toHaveClass('is-valid');
  });
});

it('should show green valid hint when file="upload.json"', async () => {
  const setNeedUpdatePanel = jest.fn();
  render(
    <TeachingCreate
      createApi="/api/teaching"
      setNeedUpdatePanel={setNeedUpdatePanel}
    />
  );
  fireEvent.change(screen.getByTestId('file-name'), {
    target: {
      files: [new File([''], 'upload.json', {type: 'application/json'})],
    },
  });
  waitFor(() => {
    expect(screen.getByTestId('file-name')).toHaveClass('is-valid');
  });
});

it('should show red invalid hint when faceboxURL="https://www.google.c"', async () => {
  const setNeedUpdatePanel = jest.fn();
  render(
    <TeachingCreate
      createApi="/api/teaching"
      setNeedUpdatePanel={setNeedUpdatePanel}
    />
  );
  fireEvent.input(screen.getByTestId('facebox-url'), {
    target: 'https://www.google.c',
  });
  waitFor(() => {
    expect(screen.getByTestId('facebox-url')).toHaveClass('is-invalid');
  });
});

it('should show red invalid hint when tagboxURL="https://www.google.c"', async () => {
  const setNeedUpdatePanel = jest.fn();
  render(
    <TeachingCreate
      createApi="/api/teaching"
      setNeedUpdatePanel={setNeedUpdatePanel}
    />
  );
  fireEvent.input(screen.getByTestId('tagbox-url'), {
    target: 'https://www.google.c',
  });
  waitFor(() => {
    expect(screen.getByTestId('tagbox-url')).toHaveClass('is-invalid');
  });
});

it('should show red invalid hint when file="upload.png"', async () => {
  const setNeedUpdatePanel = jest.fn();
  render(
    <TeachingCreate
      createApi="/api/teaching"
      setNeedUpdatePanel={setNeedUpdatePanel}
    />
  );
  fireEvent.change(screen.getByTestId('file-name'), {
    target: {
      files: [new File([''], 'upload.png', {type: 'image/png'})],
    },
  });
  waitFor(() => {
    expect(screen.getByTestId('file-name')).toHaveClass('is-invalid');
  });
});

it('should not enable reset button when inputs are empty', async () => {
  const setNeedUpdatePanel = jest.fn();
  render(
    <TeachingCreate
      createApi="/api/teaching"
      setNeedUpdatePanel={setNeedUpdatePanel}
    />
  );
  fireEvent.input(screen.getByTestId('tagbox-url'), {
    target: '',
  });
  fireEvent.input(screen.getByTestId('tagbox-url'), {
    target: '',
  });
  fireEvent.change(screen.getByTestId('file-name'), {
    target: {
      files: [],
    },
  });
  waitFor(() => {
    expect(screen.getByTestId('btn-reset')).toHaveClass('disabled');
  });
});

it('should enable reset button when any input is not empty', async () => {
  const setNeedUpdatePanel = jest.fn();
  render(
    <TeachingCreate
      createApi="/api/teaching"
      setNeedUpdatePanel={setNeedUpdatePanel}
    />
  );
  fireEvent.input(screen.getByTestId('tagbox-url'), {
    target: 'x',
  });
  fireEvent.input(screen.getByTestId('tagbox-url'), {
    target: '',
  });
  fireEvent.change(screen.getByTestId('file-name'), {
    target: {
      files: [],
    },
  });
  waitFor(() => {
    expect(screen.getByTestId('btn-reset')).not.toHaveClass('disabled');
  });
  fireEvent.input(screen.getByTestId('tagbox-url'), {
    target: '',
  });
  fireEvent.input(screen.getByTestId('tagbox-url'), {
    target: 'x',
  });
  fireEvent.change(screen.getByTestId('file-name'), {
    target: {
      files: [],
    },
  });
  waitFor(() => {
    expect(screen.getByTestId('btn-reset')).not.toHaveClass('disabled');
  });
  fireEvent.input(screen.getByTestId('tagbox-url'), {
    target: '',
  });
  fireEvent.input(screen.getByTestId('tagbox-url'), {
    target: '',
  });
  fireEvent.change(screen.getByTestId('file-name'), {
    target: {
      files: [new File([''], 'upload.png', {type: 'image/png'})],
    },
  });
  waitFor(() => {
    expect(screen.getByTestId('btn-reset')).not.toHaveClass('disabled');
  });
});

it('should enable submit button when all inputs are valid', async () => {
  const setNeedUpdatePanel = jest.fn();
  render(
    <TeachingCreate
      createApi="/api/teaching"
      setNeedUpdatePanel={setNeedUpdatePanel}
    />
  );
  fireEvent.input(screen.getByTestId('tagbox-url'), {
    target: 'https://www.google.com',
  });
  fireEvent.input(screen.getByTestId('tagbox-url'), {
    target: 'https://www.google.com',
  });
  fireEvent.change(screen.getByTestId('file-name'), {
    target: {
      files: [new File([''], 'upload.json', {type: 'application/json'})],
    },
  });
  waitFor(() => {
    expect(screen.getByTestId('btn-submit')).not.toHaveClass('disabled');
  });
});

it('should not enable submit button when any input is invalid', async () => {
  const setNeedUpdatePanel = jest.fn();
  render(
    <TeachingCreate
      createApi="/api/teaching"
      setNeedUpdatePanel={setNeedUpdatePanel}
    />
  );
  fireEvent.input(screen.getByTestId('tagbox-url'), {
    target: 'https://www.google.c',
  });
  fireEvent.input(screen.getByTestId('tagbox-url'), {
    target: 'https://www.google.com',
  });
  fireEvent.change(screen.getByTestId('file-name'), {
    target: {
      files: [new File([''], 'upload.json', {type: 'application.json'})],
    },
  });
  waitFor(() => {
    expect(screen.getByTestId('btn-submit')).not.toHaveClass('disabled');
  });
  fireEvent.input(screen.getByTestId('tagbox-url'), {
    target: 'https://www.google.com',
  });
  fireEvent.input(screen.getByTestId('tagbox-url'), {
    target: 'https://www.google.c',
  });
  fireEvent.change(screen.getByTestId('file-name'), {
    target: {
      files: [new File([''], 'upload.json', {type: 'application/json'})],
    },
  });
  waitFor(() => {
    expect(screen.getByTestId('btn-submit')).not.toHaveClass('disabled');
  });
  fireEvent.input(screen.getByTestId('tagbox-url'), {
    target: 'https://www.google.com',
  });
  fireEvent.input(screen.getByTestId('tagbox-url'), {
    target: 'https://www.google.com',
  });
  fireEvent.change(screen.getByTestId('file-name'), {
    target: {
      files: [new File([''], 'upload.png', {type: 'image/png'})],
    },
  });
  waitFor(() => {
    expect(screen.getByTestId('btn-submit')).toHaveClass('disabled');
  });
});

it('should show spinner icon on submit button after submission until get response from server', async () => {
  const setNeedUpdatePanel = jest.fn();
  render(
    <TeachingCreate
      createApi="/api/teaching"
      setNeedUpdatePanel={setNeedUpdatePanel}
    />
  );
  fireEvent.input(screen.getByTestId('tagbox-url'), {
    target: 'https://www.google.com',
  });
  fireEvent.input(screen.getByTestId('tagbox-url'), {
    target: 'https://www.google.com',
  });
  fireEvent.change(screen.getByTestId('file-name'), {
    target: {
      files: [new File([''], 'upload.json', {type: 'application/json'})],
    },
  });
  fireEvent.click(screen.getByTestId('btn-submit'));
  waitFor(() => {
    expect(screen.getByTestId('fa-spin')).toHaveClass('fa-circle-notch');
  });
});

it('should reset all inputs and buttons after get response from server', async () => {
  const setNeedUpdatePanel = jest.fn();
  render(
    <TeachingCreate
      createApi="/api/teaching"
      setNeedUpdatePanel={setNeedUpdatePanel}
    />
  );
  fireEvent.input(screen.getByTestId('tagbox-url'), {
    target: 'https://www.google.com',
  });
  fireEvent.input(screen.getByTestId('tagbox-url'), {
    target: 'https://www.google.com',
  });
  fireEvent.change(screen.getByTestId('file-name'), {
    target: {
      files: [new File([''], 'upload.json', {type: 'application/json'})],
    },
  });
  fireEvent.submit(screen.getByTestId('create-form'));
  setTimeout(() => {
    expect(screen.getByTestId('facebox-url')).not.toHaveClass('is-invalid');
    expect(screen.getByTestId('facebox-url')).not.toHaveClass('is-valid');
    expect(screen.getByTestId('facebox-url')).toHaveValue('');
    expect(screen.getByTestId('tagbox-url')).not.toHaveClass('is-invalid');
    expect(screen.getByTestId('tagbox-url')).not.toHaveClass('is-valid');
    expect(screen.getByTestId('tagbox-url')).toHaveValue('');
    expect(screen.getByTestId('btn-submit')).toHaveClass('disabled');
    expect(screen.getByTestId('fa-spin')).toHaveClass('fa-plus');
    expect(screen.getByTestId('btn-reset')).not.toHaveClass('disabled');
  }, 1500);
});
