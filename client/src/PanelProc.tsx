import React, {useEffect, useState, useRef} from 'react';
import {Card, ListGroup} from 'reactstrap';
import axios from 'axios';
import {getErrorMsg, buildIdHash, taskItem, apiType} from './utils';
import PanelHeader from './PanelHeader';
import PanelItemProc from './PanelItemProc';
import ItemStat from './ItemStat';

type PanelProcProps = {
  pollingApi: string;
  pollingIntervalSec: number;
  abortApi: string;
  type: apiType;
  updateIdHash: Function;
  needUpdatePanel: number;
};

const PanelProc = ({
  pollingApi,
  pollingIntervalSec,
  abortApi,
  type,
  updateIdHash,
  needUpdatePanel,
}: PanelProcProps) => {
  const [listElements, setListElements] = useState<JSX.Element | JSX.Element[]>(
    <ItemStat status="pending" />
  );
  const [errorMsg, setErrorMsg] = useState('');

  const useInterval = (callback: Function, delaySecond: number): void => {
    const savedCallback: React.MutableRefObject<
      Function | undefined
    > = useRef();
    const delayMillisecond = delaySecond * 1000;
    useEffect(() => {
      savedCallback.current = callback;
    }, [callback]);

    useEffect(() => {
      const timer = setInterval(() => {
        savedCallback.current();
      }, delayMillisecond);
      return () => {
        clearInterval(timer);
      };
    }),
      [callback, delayMillisecond];
  };

  const updatePanel = (): void => {
    axios
      .get(pollingApi)
      .then(response => {
        setListElements(buildElements(response.data.items));
        setErrorMsg('');
        updateIdHash(buildIdHash(response.data.items));
      })
      .catch(error => {
        setListElements(<ItemStat status="error" />);
        setErrorMsg(getErrorMsg(error));
      });
  };

  const buildElements = (
    taskItems: Array<taskItem>
  ): JSX.Element | JSX.Element[] => {
    if (taskItems.length !== 0) {
      return taskItems.map((item: taskItem, index: number) => {
        return (
          <PanelItemProc
            key={item.id}
            type={type}
            task={{index: index + 1, ...item}}
            abortApi={abortApi}
            updatePanel={updatePanel}
          />
        );
      });
    } else {
      return <ItemStat status="empty" />;
    }
  };

  useInterval(updatePanel, pollingIntervalSec);

  // update once on page load and elemetn created
  useEffect(updatePanel, [needUpdatePanel]);

  return (
    <Card className="mb-3 border-primary">
      <PanelHeader title="進行中工作列表" errorMsg={errorMsg} />
      <ListGroup flush>{listElements}</ListGroup>
    </Card>
  );
};

export default PanelProc;
