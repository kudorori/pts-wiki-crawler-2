import {waitFor, render, screen} from '@testing-library/react';
import PanelHeader from './PanelHeader';

it('should properly rendered without props', async () => {
  render(<PanelHeader />);
  await waitFor(() => {
    expect(screen.getByTestId('header-title')).toBeInTheDocument();
    expect(screen.getByTestId('header-title')).toHaveTextContent(
      'PanelHeaderTitle'
    );
    expect(screen.getByTestId('header-title')).not.toHaveTextContent(
      '發生錯誤！ '
    );
  });
});

it('should properly rendered with error message', async () => {
  render(<PanelHeader errorMsg="errmsg" />);
  await waitFor(() => {
    expect(screen.getByTestId('header-title')).toBeInTheDocument();
    expect(screen.getByTestId('header-title')).toHaveTextContent(
      'PanelHeaderTitle'
    );
    expect(screen.getByTestId('header-error')).toHaveTextContent('errmsg');
  });
});
