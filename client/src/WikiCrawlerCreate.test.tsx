import {render, screen, waitFor, fireEvent} from '@testing-library/react';
import WikiCrawlerCreate from './WikiCrawlerCreate';

it('should properly rendered without props', async () => {
  const setNeedUpdatePanel = jest.fn();
  render(
    <WikiCrawlerCreate
      createApi="/api/wiki_crawler"
      setNeedUpdatePanel={setNeedUpdatePanel}
    />
  );
  waitFor(() => {
    expect(screen.getByText(/加入新工作/)).toBeInTheDocument();
  });
});

it('should not have validation class when inputs are empty', async () => {
  const setNeedUpdatePanel = jest.fn();
  render(
    <WikiCrawlerCreate
      createApi="/api/wiki_crawler"
      setNeedUpdatePanel={setNeedUpdatePanel}
    />
  );
  waitFor(() => {
    expect(screen.getByTestId('search-url')).not.toHaveClass('is-invalid');
    expect(screen.getByTestId('search-url')).not.toHaveClass('is-valid');
    expect(screen.getByTestId('search-url')).toHaveValue('');
    expect(screen.getByTestId('depth-num')).not.toHaveClass('is-invalid');
    expect(screen.getByTestId('depth-num')).not.toHaveClass('is-valid');
    expect(screen.getByTestId('depth-num')).toHaveValue('');
    expect(screen.getByTestId('btn-submit')).toHaveClass('disabled');
    expect(screen.getByTestId('fa-spin')).toHaveClass('fa-plus');
  });
});

it('should show green valid hint when searchUrl="https://zh.wikipedia.org/wiki/中華民國"', async () => {
  const setNeedUpdatePanel = jest.fn();
  render(
    <WikiCrawlerCreate
      createApi="/api/wiki_crawler"
      setNeedUpdatePanel={setNeedUpdatePanel}
    />
  );
  fireEvent.input(screen.getByTestId('search-url'), {
    target:
      'https://zh.wikipedia.org/wiki/%E4%B8%AD%E8%8F%AF%E6%B0%91%E5%9C%8B',
  });
  waitFor(() => {
    expect(screen.getByTestId('search-url')).toHaveClass('is-valid');
  });
});

it('should show red invalid hint when searchUrl="https://zh.wikipedia.org/wiki/%E4%B8%AD%E8%8F%AF%E6%B0%91%E5%9C%8"', async () => {
  const setNeedUpdatePanel = jest.fn();
  render(
    <WikiCrawlerCreate
      createApi="/api/wiki_crawler"
      setNeedUpdatePanel={setNeedUpdatePanel}
    />
  );
  fireEvent.input(screen.getByTestId('search-url'), {
    target: 'https://zh.wikipedia.org/wiki/%E4%B8%AD%E8%8F%AF%E6%B0%91%E5%9C%8',
  });
  waitFor(() => {
    expect(screen.getByTestId('search-url')).toHaveClass('is-invalid');
  });
});

it('should show green valid hint when depthNum=3', async () => {
  const setNeedUpdatePanel = jest.fn();
  render(
    <WikiCrawlerCreate
      createApi="/api/wiki_crawler"
      setNeedUpdatePanel={setNeedUpdatePanel}
    />
  );
  fireEvent.input(screen.getByTestId('depth-num'), {
    target: '3',
  });
  waitFor(() => {
    expect(screen.getByTestId('depth-num')).toHaveClass('is-valid');
  });
});

it('should show red invalid hint when depthNum=1.1', async () => {
  const setNeedUpdatePanel = jest.fn();
  render(
    <WikiCrawlerCreate
      createApi="/api/wiki_crawler"
      setNeedUpdatePanel={setNeedUpdatePanel}
    />
  );
  fireEvent.input(screen.getByTestId('depth-num'), {
    target: '1.1',
  });
  waitFor(() => {
    expect(screen.getByTestId('depth-num')).toHaveClass('is-invalid');
  });
});

it('should show red invalid hint when depthNum=0', async () => {
  const setNeedUpdatePanel = jest.fn();
  render(
    <WikiCrawlerCreate
      createApi="/api/wiki_crawler"
      setNeedUpdatePanel={setNeedUpdatePanel}
    />
  );
  fireEvent.input(screen.getByTestId('depth-num'), {
    target: '0',
  });
  waitFor(() => {
    expect(screen.getByTestId('depth-num')).toHaveClass('is-invalid');
  });
});

it('should show red invalid hint when depthNum=-1', async () => {
  const setNeedUpdatePanel = jest.fn();
  render(
    <WikiCrawlerCreate
      createApi="/api/wiki_crawler"
      setNeedUpdatePanel={setNeedUpdatePanel}
    />
  );
  fireEvent.input(screen.getByTestId('depth-num'), {
    target: '-1',
  });
  waitFor(() => {
    expect(screen.getByTestId('depth-num')).toHaveClass('is-invalid');
  });
});

it('should enable submit button when all inputs are valid', async () => {
  const setNeedUpdatePanel = jest.fn();
  render(
    <WikiCrawlerCreate
      createApi="/api/wiki_crawler"
      setNeedUpdatePanel={setNeedUpdatePanel}
    />
  );
  fireEvent.input(screen.getByTestId('search-url'), {
    target:
      'https://zh.wikipedia.org/wiki/%E4%B8%AD%E8%8F%AF%E6%B0%91%E5%9C%8B',
  });
  fireEvent.input(screen.getByTestId('depth-num'), {
    target: '3',
  });
  waitFor(() => {
    expect(screen.getByTestId('btn-submit')).not.toHaveClass('disabled');
  });
});

it('should not enable submit button when any input is invalid', async () => {
  const setNeedUpdatePanel = jest.fn();
  render(
    <WikiCrawlerCreate
      createApi="/api/wiki_crawler"
      setNeedUpdatePanel={setNeedUpdatePanel}
    />
  );
  fireEvent.input(screen.getByTestId('search-url'), {
    target: 'https://zh.wikipedia.org/wiki/%E4%B8%AD%E8%8F%AF%E6%B0%91%E5%9C%8',
  });
  fireEvent.input(screen.getByTestId('depth-num'), {
    target: '3',
  });
  waitFor(() => {
    expect(screen.getByTestId('btn-submit')).not.toHaveClass('disabled');
  });
  fireEvent.input(screen.getByTestId('search-url'), {
    target:
      'https://zh.wikipedia.org/wiki/%E4%B8%AD%E8%8F%AF%E6%B0%91%E5%9C%8B',
  });
  fireEvent.input(screen.getByTestId('depth-num'), {
    target: '-1',
  });
  waitFor(() => {
    expect(screen.getByTestId('btn-submit')).not.toHaveClass('disabled');
  });
});

it('should show spinner icon on submit button after submission until get response from server', async () => {
  const setNeedUpdatePanel = jest.fn();
  render(
    <WikiCrawlerCreate
      createApi="/api/wiki_crawler"
      setNeedUpdatePanel={setNeedUpdatePanel}
    />
  );
  fireEvent.input(screen.getByTestId('search-url'), {
    target:
      'https://zh.wikipedia.org/wiki/%E4%B8%AD%E8%8F%AF%E6%B0%91%E5%9C%8B',
  });
  fireEvent.input(screen.getByTestId('depth-num'), {
    target: '3',
  });
  fireEvent.click(screen.getByTestId('btn-submit'));
  waitFor(() => {
    expect(screen.getByTestId('fa-spin')).toHaveClass('fa-circle-notch');
  });
});

it('should reset all inputs and buttons after get response from server', async () => {
  const setNeedUpdatePanel = jest.fn();
  render(
    <WikiCrawlerCreate
      createApi="/api/wiki_crawler"
      setNeedUpdatePanel={setNeedUpdatePanel}
    />
  );
  fireEvent.input(screen.getByTestId('search-url'), {
    target:
      'https://zh.wikipedia.org/wiki/%E4%B8%AD%E8%8F%AF%E6%B0%91%E5%9C%8B',
  });
  fireEvent.input(screen.getByTestId('depth-num'), {
    target: '3',
  });
  fireEvent.click(screen.getByTestId('btn-submit'));
  setTimeout(() => {
    expect(screen.getByTestId('facebox-url')).not.toHaveClass('is-invalid');
    expect(screen.getByTestId('search-url')).not.toHaveClass('is-invalid');
    expect(screen.getByTestId('search-url')).not.toHaveClass('is-valid');
    expect(screen.getByTestId('search-url')).toHaveValue('');
    expect(screen.getByTestId('depth-num')).not.toHaveClass('is-invalid');
    expect(screen.getByTestId('depth-num')).not.toHaveClass('is-valid');
    expect(screen.getByTestId('depth-num')).toHaveValue('');
    expect(screen.getByTestId('btn-submit')).toHaveClass('disabled');
    expect(screen.getByTestId('fa-spin')).toHaveClass('fa-plus');
  }, 1500);
});
