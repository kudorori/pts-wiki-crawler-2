import objectHash from 'object-hash';

export type endStatusMsg = {
  text: string;
  textPrefix: string;
  color: string;
};

export type fetchErr = {
  response: {
    status: number;
    data: {
      errCode: number;
      errMessage: string;
    };
  };
};

export type taskItem = {
  index?: number;
  id: number;
  createdAt?: string;
  doneAt?: string;
  searchUrl?: string;
  depthNum?: number;
  fileName?: string;
  faceBoxUrl?: string;
  tagBoxUrl?: string;
  failedMessage?: string;
  progressCount?: number;
  progressTotal?: number;
  status?: string;
};

export type apiType = 'WikiCrawler' | 'Teaching';

export function getErrorMsg(error: fetchErr): string {
  return error.response.status === 400
    ? `錯誤碼：${error.response.data.errCode}，${error.response.data.errMessage}`
    : '未知的異常，請聯絡管理者。';
}

export function getStatusDisplay(status: string): endStatusMsg {
  const statusMsgMap = new Map([
    ['FINISHED', {text: '已完成', textPrefix: '完成於', color: 'text-success'}],
    ['ABORTED', {text: '已中斷', textPrefix: '中斷於', color: 'text-danger'}],
  ]);
  return (
    statusMsgMap.get(status) || {
      text: '狀態未知',
      textPrefix: '結束於',
      color: 'text-secondary',
    }
  );
}

export function buildIdHash(itemArr: Array<taskItem>) {
  return objectHash(itemArr.map(item => item.id));
}

export function checkValidUrl(inputStr: string): boolean {
  try {
    decodeURI(inputStr);
    return /^(https?):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/.test(
      inputStr
    );
  } catch {
    return false;
  }
}

export function checkValidDepth(inputNum: number): boolean {
  return Number.isInteger(inputNum) && inputNum > 0;
}

export function checkValidFile(fileName: string): boolean {
  return (
    fileName.split('.').pop() === 'json' || fileName.split('.').pop() === 'JSON'
  );
}
