import React, {useState, useEffect} from 'react';
import {Card, ListGroup} from 'reactstrap';
import axios from 'axios';
import {getErrorMsg, taskItem, apiType} from './utils';
import PanelHeader from './PanelHeader';
import PanelItemEnded from './PanelItemEnded';
import ItemStat from './ItemStat';
import PanelFooter from './PanelFooter';
const ITEMS_PER_PAGE = 10;

type PanelEndedProps = {
  idHash: string;
  api: string;
  type: apiType;
};

const defaultProps = {
  idHash: '',
  api: '',
  type: 'WikiCrawler',
};

const PanelEnded = ({idHash, api, type}: PanelEndedProps) => {
  const [listElements, setListElements] = useState<JSX.Element | JSX.Element[]>(
    <ItemStat status="pending" />
  );
  const [errorMsg, setErrorMsg] = useState('');
  const [itemCount, setItemCount] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);

  const updatePanel = (page = currentPage): void => {
    const query = {
      page,
      pageSize: ITEMS_PER_PAGE,
    };
    axios
      .get(api, {params: query})
      .then(response => {
        setListElements(buildElements(response.data.items, type));
        setItemCount(response.data.count);
        setErrorMsg('');
      })
      .catch(error => {
        setListElements(<ItemStat status="error" />);
        setErrorMsg(getErrorMsg(error));
      });
  };

  const buildElements = (taskItems: Array<taskItem>, type: apiType) => {
    if (taskItems.length !== 0) {
      return taskItems.map((item: taskItem, index: number) => {
        return (
          <PanelItemEnded
            key={item.id}
            task={{
              index: (currentPage - 1) * ITEMS_PER_PAGE + index + 1,
              ...item,
            }}
            type={type}
          />
        );
      });
    } else {
      return <ItemStat status="empty" />;
    }
  };

  // update panel when task or page change
  useEffect(updatePanel, [idHash, currentPage]);

  return (
    <Card className="mb-3 border-primary">
      <PanelHeader title="已完成工作" errorMsg={errorMsg} />
      <ListGroup flush>{listElements}</ListGroup>
      <PanelFooter
        itemCount={itemCount}
        itemsPerPage={ITEMS_PER_PAGE}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        updatePanel={updatePanel}
      />
    </Card>
  );
};

PanelEnded.defaultProps = defaultProps;

export default PanelEnded;
