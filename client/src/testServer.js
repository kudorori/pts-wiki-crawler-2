const path = require('path');
const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(fileUpload());

app.use(express.static(path.join(__dirname, '../coverage/lcov-report')));

app.use((req, res, next) => {
  console.log(`${req.method} ${req.originalUrl}`);
  next();
});

app.get('/api/empty', (req, res) => {
  const r = {
    errCode: 0,
    errMessage: 'string',
    items: [],
  };
  res.status(200).json(r);
});

app.get('/api/wiki_crawler/process_list', (req, res) => {
  const r = {
    errCode: 0,
    errMessage: 'string',
    items: [
      {
        id: 0,
        createdAt: '1900/01/01 00:00:01',
        searchUrl: 'https://zh.wikipedia.org',
        depthNum: 1,
        status: 'PROCESSING',
        failedMessage: 'failedMsg',
        doneAt: '2020/12/31 23:59:01',
        progressCount: Math.ceil(Date.now() % 100),
        progressTotal: 100,
      },
    ],
  };
  res.status(200).json(r);
});

app.get('/api/teaching/process_list', (req, res) => {
  const r = {
    errCode: 0,
    errMessage: 'string',
    items: [
      {
        id: 0,
        createdAt: '1900/01/01 00:00:01',
        fileName: 'file01.json',
        faceBoxUrl: 'https://facebox.com',
        tagBoxUrl: 'https://tagbox.com',
        status: 'PROCESSING',
        failedMessage: 'failedMsg',
        doneAt: '2020/12/31 23:59:01',
        progressCount: Math.ceil(Date.now() % 100),
        progressTotal: 100,
      },
    ],
  };
  res.status(200).json(r);
});

app.get('/api/wiki_crawler/list', (req, res) => {
  const itemNum = 15;
  let items = [];
  const page = req.query.page !== undefined ? req.query.page : 1;
  const pageSize = req.query.pageSize !== undefined ? req.query.pageSize : 10;
  for (let i = 0; i < itemNum; i++) {
    items.push({
      id: i,
      createdAt: `1900/01/01 00:00:${i.toString().padStart(2, '0')}`,
      searchUrl: 'https://zh.wikipedia.org',
      depthNum: 1,
      status: i % 2 === 0 ? 'FINISHED' : 'ABORTED',
      failedMessage: 'failedMsg',
      doneAt: `2020/12/31 23:59:${i.toString().padStart(2, '0')}`,
    });
  }
  items = items.slice((page - 1) * pageSize, page * pageSize);
  const r = {
    errCode: 0,
    errMessage: 'errMsg',
    items: items,
    count: itemNum,
  };
  res.status(200).json(r);
});

app.get('/api/teaching/list', (req, res) => {
  const itemNum = 15;
  let items = [];
  const page = req.query.page !== undefined ? req.query.page : 1;
  const pageSize = req.query.pageSize !== undefined ? req.query.pageSize : 10;
  for (let i = 0; i < itemNum; i++) {
    items.push({
      id: i,
      createdAt: `1900/01/01 00:00:${i.toString().padStart(2, '0')}`,
      fileName: `file${i.toString().padStart(2, '0')}.json`,
      faceBoxUrl: 'https://facebox.com',
      tagBoxUrl: 'https://tagbox.com',
      status: i % 2 === 0 ? 'FINISHED' : 'ABORTED',
      failedMessage: 'failedMsg',
      doneAt: `2020/12/31 23:59:${i.toString().padStart(2, '0')}`,
    });
  }
  items = items.slice((page - 1) * pageSize, page * pageSize);
  const r = {
    errCode: 0,
    errMessage: 'errMsg',
    items: items,
    count: itemNum,
  };
  res.status(200).json(r);
});

app.post('/api/wiki_crawler', (req, res) => {
  const r = {
    errCode: 0,
    errMessage: 'string',
  };
  setTimeout(() => {
    res.status(200).json(r);
  }, 1000);
});

app.post('/api/teaching', (req, res) => {
  const r = {
    errCode: 0,
    errMessage: 'string',
  };
  setTimeout(() => {
    res.status(200).json(r);
  }, 1000);
});

app.delete('/api/wiki_crawler/list/:id', (req, res) => {
  const r = {
    errCode: 0,
    errMessage: 'string',
  };
  if (req.params.id === '0') {
    res.status(200).json(r);
  } else {
    res.status(400).json(r);
  }
});

app.delete('/api/teaching/list/:id', (req, res) => {
  const r = {
    errCode: 0,
    errMessage: 'string',
  };
  if (req.params.id === '0') {
    res.status(200).json(r);
  } else {
    res.status(400).json(r);
  }
});

app.get('/api/wiki_crawler/list/:id/download', (req, res) => {
  const r = {
    tagList: [
      {
        name: 'string',
        imageUrl: 'string',
      },
    ],
    faceList: [
      {
        name: 'string',
        imageUrl: 'string',
      },
    ],
  };
  res.json(r);
});

app.listen(4001, () => {
  console.log('Test server running at port 4001.');
});
