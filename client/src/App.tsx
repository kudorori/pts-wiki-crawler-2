import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
} from 'react-router-dom';
import {Nav, NavItem} from 'reactstrap';
import WikiCrawler from './WikiCrawler';
import Teaching from './Teaching';
import './bootstrap-custom.scss';
import '@fortawesome/fontawesome-free/css/all.min.css';

const App = () => {
  return (
    <Router>
      <div className="container pt-5">
        <Nav pills className="mb-3">
          <NavItem>
            <NavLink exact to="/" className="nav-link" activeClassName="active">
              <i className="fab fa-wikipedia-w mr-2"></i>
              維基爬蟲
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              to="/teaching"
              className="nav-link"
              activeClassName="active"
            >
              <i className="fas fa-cloud-upload-alt mr-2"></i>
              Machine Box 匯入
            </NavLink>
          </NavItem>
        </Nav>
        <Switch>
          <Route exact path="/">
            <WikiCrawler />
          </Route>
          <Route path="/teaching">
            <Teaching />
          </Route>
        </Switch>
      </div>
    </Router>
  );
};

export default App;
