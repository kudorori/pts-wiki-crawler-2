# 公視維基爬蟲 API Server

## 資料夾結構
```
|- src //主要程式
|  |- controllers // API Entry Point
|  |- entities    // DB Scheme Define Class
|  |- filters     // API Request Filter
|  |- migrations  // DB Migrations Scripts
|  |- models      // API Request & Response Type Define
|  |- repositorys // DB Repo Define
|  |- services    // Business Logic Library
|  |  |- WikiService.ts //維基爬蟲資料設定相關程式
|  |  |- WikiApiService.ts //維基爬蟲API相關程式
|  |  |- WikiQueueService.ts //維基爬蟲佇列執行相關程式
|  |  |- TeachingService.ts //MachineBox匯入資料設定程式
|  |  |- TeachingQueueService.ts //MachineBox佇列執行相關程式
|  |  |- MachineBoxService.ts //MachineBox API 相關程式
|  |- index.ts    // System Startup
|  |- Server.ts   // Server Startup
|
|- uploads        // Upload Temp Dir
|- output         // Download Temp Dir
|- envConfig.js   // Env Config Module
|- ormconfig.js   // Typeorm Config Define
```


## Web Server

> 本系統主要是使用 Ts.ED 架設 (https://tsed.io/)

## ORM

> 本系統使用的 ORM Library 為 typeorm ( https://typeorm.io/ )

## Queue Job

> 佇列排程的工作使用 Bull 來實現 (https://www.npmjs.com/package/bull)

## Environment

> 麻煩參考上層目錄的 `readme.md`

## DB Migration

> 請參考 https://typeorm.io/#/using-cli