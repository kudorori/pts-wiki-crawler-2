require('dotenv').config();

module.exports = {
  ROOT_DIR: process.cwd(),
  DB_HOST: process.env.DB_HOST,
  DB_PORT: process.env.DB_PORT,
  DB_USERNAME: process.env.DB_USER,
  DB_PASSWORD: process.env.DB_PASS,
  DB_DATABASE: process.env.DB_DATABASE,
  REDIS_HOST: process.env.REDIS_HOST,
  REDIS_PORT: parseInt(process.env.REDIS_PORT),
  REDIS_PASSWORD: process.env.REDIS_PASSWORD,
  DATE_FORMATS: {
    DATE_TIME: "yyyy-MM-dd HH:mm:ss"
  },
  PORT: process.env.PORT
}