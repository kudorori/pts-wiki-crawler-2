import { Catch, ExceptionFilterMethods, PlatformContext } from "@tsed/common";
import { Exception } from "@tsed/exceptions";
import { response } from "express";
import { BaseError } from "../exceptions/Common/BaseError";

@Catch(Error)
export class ErrorFilter implements ExceptionFilterMethods {
    catch(exception: Exception, ctx: PlatformContext) {
        console.log(exception);
        ctx.response.status(400);
        ctx.response.body(exception);
    }
}