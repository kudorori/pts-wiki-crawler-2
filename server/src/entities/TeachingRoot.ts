import { Property } from "@tsed/schema";
import { Column, Entity, OneToMany } from "typeorm";
import { TeachingItemRepo } from "../repositorys/TeachingItemRepo";
import { CommonBase } from "./Common/Base";
import { ProcessStatusEnum } from "./Common/Processing";
import { TeachingItem } from "./TeachingItem";

@Entity()
export class TeachingRoot extends CommonBase {

    @Property()
    @Column({
        nullable: true
    })
    fileName: string

    @Property()
    @Column({
        nullable: true
    })
    faceBoxUrl: string

    @Property()
    @Column({
        nullable: true
    })
    tagBoxUrl: string

    @Property()
    @Column({
        nullable: true
    })
    status: ProcessStatusEnum

    @Property()
    @Column({
        type: 'text',
        nullable: true
    })
    failedMessage: string

    @Property()
    @Column({ nullable: true })
    doneAt: Date | null


    @OneToMany(() => TeachingItem, x => x.root)
    items: TeachingItem[]
}