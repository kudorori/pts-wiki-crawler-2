import { Property } from "@tsed/schema"
import { Column, PrimaryGeneratedColumn } from "typeorm"

export class CommonBase {
    @PrimaryGeneratedColumn()
    @Property()
    id: number
    
    @Column({ nullable: true })
    @Property()
    createdAt?: Date | null

    @Column({ nullable: true })
    // @Property()
    updatedAt?: Date | null

    @Column({ nullable: true })
    // @Property()
    deletedAt?: Date | null
}