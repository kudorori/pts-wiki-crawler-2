export enum ProcessStatusEnum {
    PROCESSING = 'PROCESSING',
    SUCCESS = 'FINISHED',
    FAILED = 'ABORTED'
}