export enum ItemTypeEnum {
    UNKNOWN = "UNKNOWN",
    TAG = "TAG",
    FACE = "FACE"
}