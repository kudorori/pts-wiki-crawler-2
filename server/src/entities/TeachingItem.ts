import { Column, Entity, ManyToOne } from "typeorm";
import { CommonBase } from "./Common/Base";
import { ItemTypeEnum } from "./Common/ItemType";
import { ProcessStatusEnum } from "./Common/Processing";
import { TeachingRoot } from "./TeachingRoot";
import { WikiRoot } from "./WikiRoot";





@Entity()
export class TeachingItem extends CommonBase {
    
    @Column({ nullable: true })
    rootId: number

    @ManyToOne(() => TeachingRoot)
    root: TeachingRoot

    @Column({
        nullable: true,
        type: 'text'
    })
    image: string

    @Column({
        nullable: true
    })
    title: string

    @Column({
        nullable: true
    })
    type: ItemTypeEnum

    @Column({
        nullable: true
    })
    status: ProcessStatusEnum

    @Column({
        nullable: true
    })
    failedMessage: string
}