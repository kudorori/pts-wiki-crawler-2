import { Column, Entity, ManyToOne } from "typeorm";
import { CommonBase } from "./Common/Base";
import { ItemTypeEnum } from "./Common/ItemType";
import { ProcessStatusEnum } from "./Common/Processing";
import { WikiRoot } from "./WikiRoot";





@Entity()
export class WikiItem extends CommonBase {
    
    @Column({ nullable: true })
    rootId: number

    @ManyToOne(() => WikiRoot)
    root: WikiRoot

    @Column({ nullable: true })
    parentId: number

    @ManyToOne(() => WikiItem)
    parent?: WikiItem


    @Column({
        nullable: true
    })
    title: string

    @Column({
        nullable: true,
        type: 'text'
    })
    image: string

    @Column({
        nullable: true
    })
    searchUrl: string

    @Column({
        nullable: true
    })
    depthNum: number


    @Column({
        nullable: true
    })
    type: ItemTypeEnum

    @Column({
        nullable: true
    })
    status: ProcessStatusEnum
}