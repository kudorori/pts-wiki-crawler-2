import { Enum, Property } from "@tsed/schema";
import { BaseEntity, Column, Entity, OneToMany } from "typeorm";
import { CommonBase } from "./Common/Base";
import { ProcessStatusEnum } from "./Common/Processing";
import { WikiItem } from "./WikiItem";

@Entity()
export class WikiRoot extends CommonBase {

    @Property()
    @Column({
        nullable: true
    })
    searchUrl: string

    @Column({
        nullable: true
    })
    @Property()
    depthNum: number

    @Enum(ProcessStatusEnum)
    @Column({
        nullable: true
    })
    status: ProcessStatusEnum

    @Property()
    @Column({
        type: 'text',
        nullable: true
    })
    failedMessage: string

    @Property()
    @Column({ nullable: true })
    doneAt: Date | null
 

    @OneToMany(() => WikiItem, x => x.root)
    items: WikiItem[]
}