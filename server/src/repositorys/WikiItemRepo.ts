import { EntityRepository } from "typeorm";
import { WikiItem } from "../entities/WikiItem";
import { BaseRepo } from "./BaseRepo";


@EntityRepository(WikiItem)
export class WikiItemRepo extends BaseRepo<WikiItem> {

}