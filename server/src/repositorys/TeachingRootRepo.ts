import { EntityRepository } from "typeorm";
import { TeachingRoot } from "../entities/TeachingRoot";
import { BaseRepo } from "./BaseRepo";

@EntityRepository(TeachingRoot)
export class TeachingRootRepo extends BaseRepo<TeachingRoot> {

}