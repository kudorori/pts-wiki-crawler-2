import { FindConditions, FindManyOptions, Repository } from "typeorm";
import { PageableParams } from "../models/Params/Pageable";

export class BaseRepo<T> extends Repository<T> {
    
    getPagelabe(params: PageableParams ): FindManyOptions<T> {
        return {
            skip: params.page * params.pageSize,
            take: params.pageSize
        }
    }

    async dulicate(where: FindConditions<T>) {
        return (await this.count({ where }) > 0 )
    }
    
    async exist(id: number) {
        return (await this.count({ where: { id }})) > 0
    }

    async existByWhere(where: FindConditions<T>) {
        return (await this.count({ where })) > 0
    }
}