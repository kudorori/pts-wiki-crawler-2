import { EntityRepository } from "typeorm";
import { WikiRoot } from "../entities/WikiRoot";
import { BaseRepo } from "./BaseRepo";


@EntityRepository(WikiRoot)
export class WikiRootRepo extends BaseRepo<WikiRoot> {

}