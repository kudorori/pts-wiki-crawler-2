import { EntityRepository } from "typeorm";
import { TeachingItem } from "../entities/TeachingItem";
import { BaseRepo } from "./BaseRepo";


@EntityRepository(TeachingItem)
export class TeachingItemRepo extends BaseRepo<TeachingItem> {
    
}