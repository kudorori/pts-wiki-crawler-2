import { Configuration, Inject, PlatformApplication, Res } from "@tsed/common";
import "@tsed/platform-express";
import * as bodyParser from "body-parser";
import * as compress from "compression";
import * as cookieParser from "cookie-parser";
import * as methodOverride from "method-override";
import "@tsed/typeorm";
import "@tsed/swagger";
import "@tsed/passport";
import * as session from 'express-session'
import * as passport from "passport";
import "./filters/ErrorFilter";
import * as ormConfig from '../ormconfig.js'
import * as fileStore from 'session-file-store'
import "@tsed/platform-express";
import * as multer from "multer";
import * as envConfig from '../envConfig'
import * as path from 'path'

const FileStore = fileStore(session)
const rootDir = __dirname;



@Configuration({
  rootDir,
  statics: {
    "/": {
      root: `${envConfig.ROOT_DIR}/../client/build`
    }
  },
  multer: {
    dest: "${rootDir}/../uploads"
  },
  // acceptMimes: [
  //   "application/json",
  //   "text/plain"
  // ],
  typeorm: [
    require("../ormconfig.js")
  ],
  swagger: [
    { 
      path: "/docs", 
      specVersion: "3.0.1",
      spec: {
        info: {
          title: '',
          version: '1.0.0',
          description: `
          httpStatus = 200 的時候，表示API正常執行, 可無視errCode & errMessage\n
          httpStatus = 400 的時候，表示API有Catch到錯誤，依照errCode & errMessage 顯示錯誤訊息\n
          httpStatus = 500 的時候，表示API沒有Catch到錯誤，直接顯示未知的異常\n
          `
        }
      }
    }
  ],
  mount: {
    "/api": "${rootDir}/controllers/**/*{.ts,.js}"
  },
  componentsScan: [
    "${rootDir}/protocols/*{.ts,.js}"
  ]
})
export class Server {
  @Inject()
  app: PlatformApplication;

  @Configuration()
  settings: Configuration;

  /**
   * This method let you configure the express middleware required by your application to works.
   * @returns {Server}
   */
  public $beforeRoutesInit(): void | Promise<any> {
    this.app
      .use(cookieParser())
      .use(compress({}))
      .use(methodOverride())
      .use(bodyParser.json())
      .use(bodyParser.urlencoded({
        extended: true
      }))
      .use(session({
        store: new FileStore({
          retries: 1
        }),
        secret: "keyboard cat", // change secret key
        resave: false,
        saveUninitialized: true,
        cookie: {
          secure: false // set true if HTTPS is enabled
        }
      }))
      // .use(multer())
      ;
  }
  $afterRoutesInit() {
    this.app.get(`/*`, (req: any, res: Res) => {
      res.sendFile(path.join(envConfig.ROOT_DIR, '../client/build/index.html'));
    });
  }
}