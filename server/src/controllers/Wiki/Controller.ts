import { AcceptMime, BodyParams, Context, Controller, Delete, Get, PathParams, Post, QueryParams, Res } from "@tsed/common";
import { Format, Returns, Summary } from "@tsed/schema";
import { Binary } from "typeorm";
import { PageableParams } from "../../models/Params/Pageable";
import { ResponseCommon } from "../../models/Response/Common";
import { WikiCralwerAddBody } from "../../models/WikiCrawler/Add";
import { DownloadBody } from "../../models/WikiCrawler/Download";
import { WikiCrawlerListResponse, WikiCrawlerProcessListResponse } from "../../models/WikiCrawler/ProcessList";
import { WikiService } from "../../services/WikiService";
import { createReadStream, ReadStream } from 'fs'

@Controller("/wiki_crawler")
export class WikiCrawlerController {

    constructor(
        private service: WikiService
    ) {

    }

    @Get("/process_list")
    @Summary("取得目前正在進行的列表")
    @Returns(200, WikiCrawlerProcessListResponse)
    async GetProcessList(): Promise<WikiCrawlerProcessListResponse> {
        return {
            items: await this.service.getProcessList(),
            errCode: 200,
            errMessage: undefined
        }
    }

    @Get("/list")
    @Summary("取得已完成的列表")
    @Returns(200, WikiCrawlerListResponse)
    async GetList(@QueryParams() params: PageableParams): Promise<WikiCrawlerListResponse> {
        return {
            errCode: 200,
            errMessage: undefined,
            ...(await this.service.getList(params)),
        }
    }

    @Post()
    @Summary("新增工作")
    @Returns(200, ResponseCommon)
    async AddProcess(@BodyParams() data: WikiCralwerAddBody): Promise<ResponseCommon> {
        console.log(data)
        await this.service.addRoot(data)
        return {
            errCode: 200
        }
    }

    @Delete("/list/:id")
    @Summary("中止工作")
    @Returns(200, ResponseCommon)
    async StopProcess(@PathParams("id") id: number): Promise<ResponseCommon> {
        await this.service.stop(id)
        return {
            errCode: 200
        }
    }


    @Get("/list/:id/download")
    @Summary("匯出資料[網頁端直接 <a download>..</a> 下載]")
    async Download(@PathParams("id") id: number, @Res() res: Res)  {
        try {
            const filePath = await this.service.download(id)
            res.download(filePath, err => {
                console.log(err)
            })
        } catch ( err ) {
            console.log(err)
            throw err
        }
        // return createReadStream(filePath)
    }

}