import { BodyParams, Controller, Delete, Get, MultipartFile, PathParams, Post, QueryParams } from "@tsed/common";
import { In, Required, Returns, Summary } from "@tsed/schema";
import { PageableParams } from "../../models/Params/Pageable";
import { ResponseCommon } from "../../models/Response/Common";
import { TeachingAddBody } from "../../models/Teaching/Add";
import { TeachingListResponse, TeachingProcessListResponse } from "../../models/Teaching/ProcessList";
import { TeachingService } from "../../services/TeachingService";

@Controller("/teaching")
export class TeachingController {

    constructor(
        private service: TeachingService
    ) {

    }


    @Get("/process_list")
    @Summary("取得目前正在進行的列表")
    @Returns(200, TeachingProcessListResponse)
    async GetProcessList(): Promise<TeachingProcessListResponse> {
        return {
            items: await this.service.getProcessList(),
            errCode: 200,
            errMessage: undefined
        }
    }

    @Get("/list")
    @Summary("取得已完成的列表")
    @Returns(200, TeachingListResponse)
    async GetList(@QueryParams() params: PageableParams): Promise<TeachingListResponse> {
        return {
            ...await this.service.getList(params),
            errCode: 200,
            errMessage: undefined
        }
    }

    @Post()
    @Summary("新增工作")
    @Returns(200, ResponseCommon)
    async AddProcess(
        @Required() @MultipartFile("file") file: Express.Multer.File,
        @Required() @BodyParams("faceBoxUrl") faceBoxUrl: string,
        @Required() @BodyParams("tagBoxUrl") tagBoxUrl: string,
    ): Promise<ResponseCommon> {
        console.log(file, faceBoxUrl, tagBoxUrl);
        await this.service.add({
            file,
            faceBoxUrl,
            tagBoxUrl
        })
        return {
            errCode: 200,
        }
    }

    @Delete("/list/:id")
    @Summary("中止工作")
    @Returns(200, ResponseCommon)
    async StopProcess(@PathParams("id") id: number): Promise<ResponseCommon> {
        await this.service.stop(id);
        return {
            errCode: 200,
            errMessage: undefined
        }
    }
}