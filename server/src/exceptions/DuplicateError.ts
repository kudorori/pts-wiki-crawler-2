import { BaseError } from "./Common/BaseError";

export class DuplicateError extends BaseError {
    constructor() {
        super(400, '資料重覆')
    }
}