import { BaseError } from "./Common/BaseError";

export class RequireError extends BaseError {
    constructor(fieldName: string) {
        super(400, `${fieldName} is Required`)
    }
}