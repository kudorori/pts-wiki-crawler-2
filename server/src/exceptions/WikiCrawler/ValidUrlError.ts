import { BaseError } from "../Common/BaseError";

export class WikiCrawlerValidUrlError extends BaseError {
    constructor(prefix: string) {
        super(400, `Wiki 關鍵字無效`)
    }
}