import { BaseError } from "./Common/BaseError";

export class NotExistError extends BaseError {
    constructor() {
        super(400, "Not Exist");
    }
}