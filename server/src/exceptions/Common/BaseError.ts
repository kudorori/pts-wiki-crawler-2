export class BaseError extends Error {
    constructor(public errCode: number, public errMessage?: string) {
        super(errMessage)
    }
}