import { BaseError } from "../Common/BaseError";

export class TeachingValidUrlError extends BaseError {
    constructor(prefix: string) {
        super(400, `${prefix} 網址無效`)
    }
}