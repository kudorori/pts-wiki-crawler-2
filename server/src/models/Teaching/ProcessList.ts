import { CollectionOf, Property } from "@tsed/schema";
import { property } from "lodash";
import { ProcessStatusEnum } from "../../entities/Common/Processing";
import { TeachingRoot } from "../../entities/TeachingRoot";
import { ResponseItems, ResponsePageable } from "../Response/Items";

export class TeachingProgressListItem {
    @Property()
    id: number
    @Property()
    createdAt: string

    @Property()
    updatedAt: string

    @Property()
    fileName: string

    @Property()
    faceBoxUrl: string

    @Property()
    tagBoxUrl: string

    @Property()
    status: ProcessStatusEnum

    @Property()
    failedMessage: string

    @Property()
    doneAt: string
}

export class TeachingProgressItem extends TeachingProgressListItem {
    @Property()
    progressCount: number
    @Property()
    progressTotal: number
}


export class TeachingListResponse extends ResponsePageable<TeachingProgressListItem> {
    @CollectionOf(TeachingProgressListItem)
    items: TeachingProgressListItem[]
}


export class TeachingProcessListResponse extends ResponseItems<TeachingProgressItem> {
    @CollectionOf(TeachingProgressItem)
    items: TeachingProgressItem[]
}