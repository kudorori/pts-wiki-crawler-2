import { MultipartFile } from "@tsed/common";
import { Format, Property, Required } from "@tsed/schema";

export class TeachingAddBody {

    file: Express.Multer.File
    faceBoxUrl: string
    tagBoxUrl: string
}