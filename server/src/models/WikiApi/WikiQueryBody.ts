export interface WikiApiQueryPageImageBody {
  batchcomplete: string;
  query: {
    pages: {
      [key: string]: {
        pageimage: string
      }
    }
  };
}
