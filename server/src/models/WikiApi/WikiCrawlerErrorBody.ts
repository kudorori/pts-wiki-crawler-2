export interface WikiApiErrorBody {
  error:    Error;
  servedby: string;
}

interface Error {
  code: string;
  info: string;
  "*":  string;
}


export const hasError = (item: any): item is WikiApiErrorBody => {
  return item.error !== undefined
}