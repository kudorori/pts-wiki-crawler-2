export interface WikiApiParseBody {
  parse?: Parse;
}

export interface Parse {
  title:         string;
  pageid:        number;
  parsedsummary: Parsedsummary;
  links?:         Link[];
  images?:        string[];
  categories: {
    '*': string
  }[]
}

export interface Link {
  ns:      number;
  exists?: string;
  "*":     string;
}

export interface Parsedsummary {
  "*": string;
}
