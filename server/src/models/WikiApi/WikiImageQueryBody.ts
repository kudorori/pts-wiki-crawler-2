export interface WikiApiImageQueryBody {
  batchcomplete: string;
  query:         Query;
}

export interface Query {
  pages: Pages;
}

export interface Pages {
  [key: string]: The1;
}

export interface The1 {
  ns:              number;
  title:           string;
  missing:         string;
  known:           string;
  imagerepository: string;
  imageinfo:       Imageinfo[];
}

export interface Imageinfo {
  url:                 string;
  descriptionurl:      string;
  descriptionshorturl: string;
  size: number,
  mime: string
}
