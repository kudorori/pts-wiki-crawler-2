import { Property } from "@tsed/schema";
import { PageableResult } from "../Pageable/Result";
import { ResponseCommon } from "./Common";

export class ResponseItems<T = any> extends ResponseCommon {
    
    @Property()
    items: T[]
}


export class ResponsePageable<T = any> extends ResponseItems<T> implements PageableResult<T> {

    @Property()
    count: number
}