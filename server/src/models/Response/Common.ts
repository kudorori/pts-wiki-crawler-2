import { Property } from "@tsed/schema"

export class ResponseCommon {
    @Property()
    errCode: number

    @Property()
    errMessage?: string
}