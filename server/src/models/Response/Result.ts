import { Property } from "@tsed/schema";
import { ResponseCommon } from "./Common";

export class ResponseResult<T> extends ResponseCommon {
    
    @Property()
    result: T
}