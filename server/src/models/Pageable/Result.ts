import { CollectionOf, Property } from "@tsed/schema"

export interface PageableResult<T> {
    count: number
    items: T[]
}