import { CollectionOf, Property } from "@tsed/schema";
import { ProcessStatusEnum } from "../../entities/Common/Processing";
import { WikiRoot } from "../../entities/WikiRoot";
import { ResponseItems, ResponsePageable } from "../Response/Items";



export class WikiCrawlerListItem {
    @Property() 
    id: number

    @Property()
    searchUrl: string

    @Property()
    depthNum: number

    @Property()
    status: ProcessStatusEnum

    @Property()
    failedMessage: string

    @Property()
    doneAt: string
    
    @Property()
    createdAt: string
    
    @Property()
    updatedAt: string

    
}

export class WikiCrawlerProgressItem extends WikiCrawlerListItem {
    @Property()
    progressCount: number

    @Property()
    progressTotal: number
}

export class WikiCrawlerListResponse extends ResponsePageable<WikiCrawlerListItem> {
    @CollectionOf(WikiCrawlerListItem)
    items: WikiCrawlerListItem[]
}


export class WikiCrawlerProcessListResponse extends ResponseItems<WikiCrawlerProgressItem> {
    @CollectionOf(WikiCrawlerProgressItem)
    items: WikiCrawlerProgressItem[]
}