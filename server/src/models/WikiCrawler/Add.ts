import { Property, Required } from "@tsed/schema";

export class WikiCralwerAddBody {
    @Property()
    @Required()
    searchUrl: string

    @Property()
    @Required()
    depthNum: number
}