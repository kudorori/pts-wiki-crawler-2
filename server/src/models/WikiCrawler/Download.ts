import { CollectionOf, Enum, Property } from "@tsed/schema";
import { ItemTypeEnum } from "../../entities/Common/ItemType";




export class DownloadItem {
    @Property()
    name: string

    @Property()
    imageUrl: string
}

export class DownloadBody {
    @CollectionOf(DownloadItem)
    tagList: DownloadItem[]

    @CollectionOf(DownloadItem)
    faceList: DownloadItem[]
}