import { Property } from "@tsed/schema"

export class PageableParams {
    @Property()
    page: number
    
    @Property()
    pageSize: number
}