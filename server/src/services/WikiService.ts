import { Service } from "@tsed/di";
import { ProcessStatusEnum } from "../entities/Common/Processing";
import { WikiItem } from "../entities/WikiItem";
import { WikiRoot } from "../entities/WikiRoot";
import { WikiCralwerAddBody } from "../models/WikiCrawler/Add";
import { WikiCrawlerListItem, WikiCrawlerListResponse, WikiCrawlerProcessListResponse, WikiCrawlerProgressItem } from "../models/WikiCrawler/ProcessList";
import { WikiItemRepo } from "../repositorys/WikiItemRepo";
import { WikiRootRepo } from "../repositorys/WikiRootRepo";
import { BaseService } from "./BaseService";
import { WikiApiService } from "./WikiApiService";
import { WikiQueueService } from "./WikiQueueService";
import { PageableParams } from "../models/Params/Pageable";
import { PageableResult } from "../models/Pageable/Result";
import { Not } from "typeorm";
import { ROOT_DIR, DATE_FORMATS } from '../../envConfig'
import * as bfj from 'bfj'
import { ItemTypeEnum } from "../entities/Common/ItemType";
import * as dateFns from 'date-fns'
import axios from 'axios'
import { WikiCrawlerValidUrlError } from "../exceptions/WikiCrawler/ValidUrlError";
import * as _ from 'lodash'

@Service()
export class WikiService extends BaseService {

    constructor(
        private wikiRootRepo: WikiRootRepo,
        private wikiItemRepo: WikiItemRepo,
        private queue: WikiQueueService,
        private wikiApi: WikiApiService
    ) {
        super();
    }

    /**
     * 驗證網址
     * @param url 
     */
    private async validSiteExists(url: string) {
        try {
            await axios.get(url, {
                timeout: 3000
            })
        } catch ( err ) {
            throw new WikiCrawlerValidUrlError("Wiki Url")
        }
    }

    /**
     * 取得所有項目
     * @param params 
     */
    async getList(params: PageableParams): Promise<PageableResult<WikiCrawlerListItem>> {
        const { page, pageSize } = params

        const [ items, count ] = await this.wikiRootRepo.findAndCount({
            where: {
                status: Not(ProcessStatusEnum.PROCESSING)
            },
            order: {
                id: "DESC"
            },
            skip: (page - 1) * pageSize,
            take: pageSize
        })

        return {
            items: items.map((x) => ({
                ...x,
                createdAt: dateFns.format(x.createdAt, DATE_FORMATS.DATE_TIME),
                updatedAt: dateFns.format(x.updatedAt, DATE_FORMATS.DATE_TIME),
                doneAt: dateFns.format(x.doneAt, DATE_FORMATS.DATE_TIME)
            })),
            count
        }
    }

    /**
     * 取得正在進行中的項目
     */
    async getProcessList(): Promise<WikiCrawlerProgressItem[]> {
        const items = await this.wikiRootRepo.find({
            where: { 
                status: ProcessStatusEnum.PROCESSING
            }
        })

        let processCountList = await this.wikiItemRepo.createQueryBuilder('item')
            .select(`item.rootId as rootId, COUNT(*) as count`)
            .groupBy(`item.rootId`)
            .where('item.status = :status', { status: ProcessStatusEnum.PROCESSING })
            .getRawMany()

        let allCountList = await this.wikiItemRepo.createQueryBuilder('item')
            .select(`item.rootId as rootId, COUNT(*) as count`)
            .groupBy(`item.rootId`)
            .getRawMany()

        let processCountListByRootId = _.keyBy(processCountList, 'rootId')
        let allCountListByRootId = _.keyBy(allCountList, 'rootId')


        return items.map(x => {
            let progressTotal = parseInt(_.get(allCountListByRootId, [ x.id, 'count'], 1));
            let progressCount = parseInt(_.get(processCountListByRootId,[ x.id, 'count'], 0));
            
            return {
                ...x,
                progressCount: progressTotal - progressCount,
                progressTotal,
                createdAt: dateFns.format(x.createdAt, DATE_FORMATS.DATE_TIME),
                updatedAt: '',
                doneAt: ''
            }
        })
    }

    /**
     * 新增作業
     * @param data 
     */
    async addRoot(data: WikiCralwerAddBody) {
        

        let rootDbData = new WikiRoot();
        rootDbData.depthNum = data.depthNum;
        rootDbData.searchUrl = data.searchUrl.includes('http') ? data.searchUrl : `https://zh.wikipedia.org/wiki/${encodeURI(data.searchUrl)}`;
        rootDbData.status = ProcessStatusEnum.PROCESSING;
        console.log(rootDbData.searchUrl)
        await this.validSiteExists(rootDbData.searchUrl)

        this.bindCreatedAt(rootDbData);
        
        try {
            rootDbData = await this.wikiRootRepo.save(rootDbData);
            await this.addItem(rootDbData.id, null, rootDbData.searchUrl, 1)
            if ( await this.queue.getWorkerCount() === 0 ) {
                await this.queue.start(rootDbData.id)
            }
        } catch ( err ) {
            rootDbData.failedMessage = `Wiki Api Fetch Error`
            rootDbData.status = ProcessStatusEnum.FAILED;
            await this.wikiRootRepo.save(rootDbData)
        }
        
        
    }

    /**
     * 新增子項目
     * @param rootId 
     * @param parentId 
     * @param url 
     * @param depthNum 
     */
    private async addItem(rootId: number, parentId: number | null, url: string, depthNum: number ) {
        const title = this.wikiApi.getWikiTitle(url)
        
        let itemDbData = new WikiItem();
        itemDbData.rootId = rootId;
        itemDbData.parentId = parentId;
        itemDbData.status = ProcessStatusEnum.PROCESSING;
        itemDbData.searchUrl = url;
        itemDbData.title = title;
        itemDbData.depthNum = depthNum;
        await this.wikiItemRepo.manager.save(itemDbData)
    }

    /**
     * 停止作業
     * @param rootId 
     */
    async stop(rootId: number) {
        let dbData = await this.wikiRootRepo.findOne(rootId)
        dbData.status = ProcessStatusEnum.FAILED;
        dbData.doneAt = new Date()
        dbData.failedMessage = "手動中斷"
        await this.wikiItemRepo.update({
            rootId: rootId,
        }, { 
            status: ProcessStatusEnum.FAILED,
        })
        await this.wikiRootRepo.save(dbData)
    }


    /**
     * 匯出檔案
    */
    async download(rootId: number): Promise<string> {
        
        const path = `${ROOT_DIR}/output/${rootId}.json`;
        const items = await this.wikiItemRepo.find({
            where: {
                rootId,
                status: ProcessStatusEnum.SUCCESS
            }
        })

        

        let outputJSON = {
            tagList: [],
            faceList: []
        }

        for ( const item of items ) {
            if ( item.image !== undefined && item.image !== null && item.image !== "") {
                const lowerUrl = item.image.toLowerCase()

                if ( !(lowerUrl.includes('.svg')) ) {
                    if ( item.type === ItemTypeEnum.FACE ) {
                        outputJSON.faceList.push({
                            title: item.title,
                            image: item.image,
                            httplink: `https://zh.wikipedia.org/wiki/${item.title}`
                        })
                    } else if ( item.type === ItemTypeEnum.TAG ) {
                        outputJSON.tagList.push({
                            title: item.title,
                            image: item.image,
                            httplink: `https://zh.wikipedia.org/wiki/${item.title}`
                        })
                    }
                }
            }
            
            
        }

        await bfj.write(path, outputJSON)

        return path
    }
}