import { BaseService } from "./BaseService";
import { RedisOptions } from "ioredis";
import * as Config from '../../envConfig';
import * as Queue from 'bull'
import { Inject, OnInit } from "@tsed/di";
import { Logger } from "@tsed/common";

export class BaseQueueService extends BaseService implements OnInit {
    @Inject()
    logger: Logger
    protected redisConfig: RedisOptions = {
        host: Config.REDIS_HOST,
        port: Config.REDIS_PORT,
        password: Config.REDIS_PASSWORD
    }

    private cleanKeys: Queue.JobStatusClean[] = ['completed', 'wait', 'active', 'paused', 'delayed', 'failed']

    public Queue: Queue.Queue

    async getWorkerCount() {
        const count = await this.Queue.count()
        console.log(this.Queue.name, `now worker count = ${count}`)
        return count
    }

    async clearQueue() {
        for (const status of this.cleanKeys) {
            await this.Queue.clean(0, status)
        }
    }

    async $onInit() {
        // return this.clearQueue()
    }
}