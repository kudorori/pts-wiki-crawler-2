import { Logger } from "@tsed/common";
import { Inject, Service } from "@tsed/di";
import axios from 'axios'

type TeachBaseRes = { success: boolean, error: string }


@Service()
export class MachineBoxService {

    @Inject()
    logger: Logger;
    private async commonTeach(host: string, url: string, title: string, image: string ) {
        
        console.log(`Teach ${host} ${url} / ${title} = ${image}`)
        try {
            const res = await axios.request<TeachBaseRes>({
                url: url,
                baseURL: host,
                method: 'POST',
                data: {
                    name: title,
                    tag: title,
                    url: image
                }
            })
            if ( !res.data.success ) {
                throw new Error(res.data.error)
            }

        } catch ( err ) {
            if ( err.response.data.error !== undefined ) {
                throw new Error(err.response.data.error)
            }
            throw new Error(err.message)
        }
    }

    /**
     * tagbox 教學
     * @param host 
     * @param title 
     * @param image 
     */
    async teachTag(host: string, title: string, image: string) {
        await this.commonTeach(host, '/tagbox/teach', title, image)
    }

    /**
     * facebox 教學
     * @param host 
     * @param title 
     * @param image 
     */
    async teachFace(host: string, title: string, image: string) {
        await this.commonTeach(host, '/facebox/teach', title, image)
    }
}