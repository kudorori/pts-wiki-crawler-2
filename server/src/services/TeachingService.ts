import { Service } from "@tsed/di";
import { TeachingAddBody } from "../models/Teaching/Add";
import * as bfj from 'bfj'
import { BaseService } from "./BaseService";
import { TeachingRootRepo } from "../repositorys/TeachingRootRepo";
import { TeachingItemRepo } from "../repositorys/TeachingItemRepo";
import { TeachingRoot } from "../entities/TeachingRoot";
import { ProcessStatusEnum } from "../entities/Common/Processing";
import { TeachingItem } from "../entities/TeachingItem";
import { ItemTypeEnum } from "../entities/Common/ItemType";
import { TeachingQueueService } from "./TeachingQueueService";
import { PageableParams } from "../models/Params/Pageable";
import { Not } from "typeorm";
import { partition } from "lodash";
import axios from 'axios'
import { TeachingValidUrlError } from "../exceptions/Teaching/ValidUrlError";
import { PageableResult } from "../models/Pageable/Result";
import { TeachingProgressItem, TeachingProgressListItem } from "../models/Teaching/ProcessList";
import { DATE_FORMATS } from "../../envConfig";
import * as dateFns from 'date-fns'
type FileJsonData = {
    tagList: {
        image: string,
        title: string
    }[],
    faceList: {
        image: string,
        title: string
    }[]
}


@Service()
export class TeachingService extends BaseService {

    constructor(
        private rootRepo: TeachingRootRepo,
        private itemRepo: TeachingItemRepo,
        private queue: TeachingQueueService
    ) {
        super()
    }

    /**
     * 驗證網址
     * @param url 
     */
    private async validSiteExists(url: string) {
        try {
            await axios.get(url, {
                timeout: 3000
            })
        } catch ( err ) {
            throw new Error("網址無效 404")
        }
    }
    
    /**
     * Parser Json Data
     * @param rootId 
     * @param type 
     * @param items 
     */
    private getItems(
        rootId: number,
        type: ItemTypeEnum,
        items: { image: string, title: string }[]
    ) {
        return items.map(x => {
            let data = new TeachingItem();
            data.title = x.title;
            data.image = x.image;
            data.type = type;
            data.rootId = rootId;
            data.status = ProcessStatusEnum.PROCESSING;
            data = this.bindCreatedAt(data);
            return data
        })
    }   


    /**
     * 新增項目
     * @param data 
     */
    async add(data: TeachingAddBody) {
        const { file, faceBoxUrl, tagBoxUrl } = data;
        const jsonData: FileJsonData = await bfj.read(file.path);

        try {
            await this.validSiteExists(tagBoxUrl)
        } catch ( err ) {
            throw new TeachingValidUrlError("TagBox")
        }

        try {
            await this.validSiteExists(faceBoxUrl)
        } catch ( err ) {
            throw new TeachingValidUrlError("FaceBox")
        }

        let rootData = new TeachingRoot();
        rootData.fileName = file.originalname;
        rootData.status = ProcessStatusEnum.PROCESSING;
        rootData.tagBoxUrl = tagBoxUrl;
        rootData.faceBoxUrl = faceBoxUrl;
        rootData = this.bindCreatedAt(rootData);
        rootData = await this.rootRepo.save(rootData);

        


        let items = [
            ...this.getItems(rootData.id, ItemTypeEnum.FACE, jsonData.faceList),
            ...this.getItems(rootData.id, ItemTypeEnum.TAG, jsonData.tagList),
        ]

        await this.itemRepo.insert(items)
        
        if ( await this.queue.getWorkerCount() === 0 ) {
            this.queue.start(rootData.id);
        }

    }

    /**
     * 中斷佇列執行
     * @param rootId 
     */
    async stop(rootId: number) {
        let dbData = await this.rootRepo.findOne(rootId)
        dbData.status = ProcessStatusEnum.FAILED;
        dbData.failedMessage = "手動中斷"
        dbData.doneAt = new Date()
        await this.itemRepo.update({
            rootId: rootId,
        }, { 
            status: ProcessStatusEnum.FAILED
        })
        await this.rootRepo.save(dbData)
    }

    /**
     * 取得目前正在進行的項目
     */
    async getProcessList(): Promise<TeachingProgressItem[]> {
        const items = await this.rootRepo.find({
            where: { 
                status: ProcessStatusEnum.PROCESSING
            },
            relations: ['items']
        })

        return items.map(x => {
            const [ doneList, processList ] = partition(x.items, x => x.status !== ProcessStatusEnum.PROCESSING )
            return {
                ...x,
                progressCount: doneList.length,
                progressTotal: x.items.length,
                createdAt: dateFns.format(x.createdAt, DATE_FORMATS.DATE_TIME),
                updatedAt: '',
                doneAt: ''
            }
        })
    }
    /**
     * 取得所有項目
     * @param rootId 
     * @param type 
     * @param items 
     */
    async getList(params: PageableParams): Promise<PageableResult<TeachingProgressListItem>> {
        const { page, pageSize } = params

        const [ items, count ] = await this.rootRepo.findAndCount({
            where: {
                status: Not(ProcessStatusEnum.PROCESSING)
            },
            order: {
                id: "DESC"
            },
            skip: (page - 1) * pageSize,
            take: pageSize
        })

        return {
            items: items.map(x => ({
                ...x,
                createdAt: dateFns.format(x.createdAt, DATE_FORMATS.DATE_TIME),
                updatedAt: dateFns.format(x.updatedAt, DATE_FORMATS.DATE_TIME),
                doneAt: dateFns.format(x.doneAt, DATE_FORMATS.DATE_TIME)
            })),
            count
        }
    }

    
}