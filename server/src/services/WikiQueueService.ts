import { OnReady } from "@tsed/common";
import { OnInit, Service } from "@tsed/di";
import * as Queue from "bull";
import { ItemTypeEnum } from "../entities/Common/ItemType";
import { ProcessStatusEnum } from "../entities/Common/Processing";
import { WikiItem } from "../entities/WikiItem";
import { WikiItemRepo } from "../repositorys/WikiItemRepo";
import { WikiRootRepo } from "../repositorys/WikiRootRepo";
import { BaseQueueService } from './BaseQueueService'
import { WikiApiService } from "./WikiApiService";
import { MoreThan } from "typeorm"


@Service()
export class WikiQueueService extends BaseQueueService implements OnInit, OnReady {
    
    constructor(
        private wikiRootRepo: WikiRootRepo,
        private wikiItemRepo: WikiItemRepo,
        private WikiApiService: WikiApiService
    ) {
        super();
        this.Queue = new Queue('WikiQueue', {
            redis: this.redisConfig
        })
    }

    private async $initialWorker() {
        const firstRoot = await this.wikiRootRepo.findOne({
            where: { 
                status: ProcessStatusEnum.PROCESSING
            }
        })
        if ( firstRoot !== undefined ) {
            this.start(firstRoot.id)
        }
        
    }

    /**
     * 佇列執行程式
     * @param job 
     * @param done 
     */
    private async $processQueue(job: Queue.Job<number>, done: Queue.DoneCallback ) {
        console.log(`${this.Queue.name} run Queue, job data = ${job.data}`)
        try {
            const data = await this.wikiItemRepo.findOne(job.data, {
                relations: ['root']
            })
            if ( data.root.status === ProcessStatusEnum.PROCESSING )  {
                console.log(`${this.Queue.name} run data.title = ${data.title}`)
                const wikiPage = await this.WikiApiService.getWikiPage(data.title)
                if ( wikiPage !== undefined ) {
                    if ( data.depthNum < data.root.depthNum ) {
                        const newItems = wikiPage.links.map(x => {
                            let item = new WikiItem()
                            item.depthNum = data.depthNum + 1;
                            item.title = x
                            item.status = ProcessStatusEnum.PROCESSING;
                            item.parentId = data.id;
                            item.rootId = data.rootId; 
                            return item
                        })
                        await this.wikiItemRepo.insert(newItems)
                    }   
    
                    data.image = wikiPage.pageImage;
                    
                    if ( wikiPage !== undefined ) {
                        data.type = wikiPage.isPeople ? ItemTypeEnum.FACE : ItemTypeEnum.TAG;
                        data.status = ProcessStatusEnum.SUCCESS;
                    } else {
                        data.status = ProcessStatusEnum.FAILED;
                        data.type = ItemTypeEnum.UNKNOWN
                    }
                    
                    await this.wikiItemRepo.save(data)
                    
                } else {
                    await this.wikiItemRepo.update({
                        id: data.id,
                    }, {
                        status: ProcessStatusEnum.FAILED
                    })
                }
            }
            
            if ( await this.getWorkerCount() === 0 ) {
                this.start(data.rootId);
            }
            
        } catch ( err ) {
            console.log(err.message)
        }
        
        
        done()
    }


    async $onInit() {
        await this.clearQueue()
        this.$initialWorker();
        this.Queue.process(this.$processQueue.bind(this));
    }

    async $onReady() {
        
    }

    /**
     * 佇列啟動程式 (預設每次取20筆工作加入佇列)
     */
    async start(rootId: number) {
        let items = await this.wikiItemRepo.find({
            where: {
                rootId,
                status: ProcessStatusEnum.PROCESSING
            },
            take: 20
        })
        console.log(`${this.Queue.name} start rootId=${rootId}, wait run items count = ${items.length}`)
        if ( items.length > 0 ) {
            this.Queue.addBulk(items.map(x => ({
                data: x.id
            })))
        } else {
            await this.wikiRootRepo.update({
                id: rootId,
                status: ProcessStatusEnum.PROCESSING
            }, {
                status: ProcessStatusEnum.SUCCESS,
                doneAt: new Date()
            })

            const nextRoot = await this.wikiRootRepo.findOne({
                where: {
                    id: MoreThan(rootId),
                    status: ProcessStatusEnum.PROCESSING
                }
            })

            if ( nextRoot !== undefined ) {
                this.start(nextRoot.id)
            }
        }

        
        
    }

    

}   