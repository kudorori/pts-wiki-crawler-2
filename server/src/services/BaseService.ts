import { FindConditions } from "typeorm";
import { CommonBase } from "../entities/Common/Base";
import { DuplicateError } from "../exceptions/DuplicateError";
import { NotExistError } from "../exceptions/NotExistError";
import { BaseRepo } from "../repositorys/BaseRepo";

export class BaseService {
    bindCreatedAt<T extends CommonBase>(data: T) {
        data.createdAt = new Date();
        data.updatedAt = new Date();
        return data
    }

    bindUpdatedAt<T extends CommonBase>(data: T) {
        data.updatedAt = new Date();
        return data;
    }

    bindDeletedAt<T extends CommonBase>(data: T) {
        data.deletedAt = new Date();
        return data;
    }


    async checkExist<T>(repo: BaseRepo<T>, id: number) {
        if ( !(await repo.exist(id)) ) {
            throw new NotExistError();
        }
    }

    async checkExistByWhere<T>(repo: BaseRepo<T>,  where: FindConditions<T>) {
        if ( !(await repo.existByWhere(where)) ) {
            throw new NotExistError();
        }
    }


    async checkDulicate<T>(repo: BaseRepo<T>, where: FindConditions<T> ) {
        if ( (await repo.dulicate(where)) ) {
            throw new DuplicateError()
        }
    }
}