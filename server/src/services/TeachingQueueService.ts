import { OnReady } from "@tsed/common";
import { OnInit, Service } from "@tsed/di";
import { MoreThan } from "typeorm";
import { ProcessStatusEnum } from "../entities/Common/Processing";
import { TeachingRoot } from "../entities/TeachingRoot";
import { TeachingItemRepo } from "../repositorys/TeachingItemRepo";
import { TeachingRootRepo } from "../repositorys/TeachingRootRepo";
import { BaseQueueService } from "./BaseQueueService";
import * as Queue from "bull";
import { MachineBoxService } from "./MachineBoxService";
import { ItemTypeEnum } from "../entities/Common/ItemType";


@Service()
export class TeachingQueueService extends BaseQueueService implements OnInit, OnReady {
    constructor(
        private itemRepo: TeachingItemRepo,
        private rootRepo: TeachingRootRepo,
        private machineBoxApi: MachineBoxService
    ) {
        super()
        this.Queue = new Queue('TeachingQueue', {
            redis: this.redisConfig
        })
    }

    async $initialWorker() {
        const firstRoot = await this.rootRepo.findOne({
            where: { 
                status: ProcessStatusEnum.PROCESSING
            }
        })
        if ( firstRoot !== undefined ) {
            this.start(firstRoot.id)
        }
    }
    async $onInit() {
        await this.clearQueue()
        this.$initialWorker();
        this.Queue.process(this.$processQueue.bind(this));
    }

    $onReady() {

    }

    /**
     * 佇列執行程式
     * @param job 
     * @param done 
     */
    async $processQueue(job: Queue.Job<number>, done: Queue.DoneCallback ) {
        const data = await this.itemRepo.findOne(job.data, {
            relations: ['root']
        })

        if ( data.root.status === ProcessStatusEnum.PROCESSING ) {
            try {
                if ( data.type === ItemTypeEnum.FACE ) {
                    await this.machineBoxApi.teachFace(data.root.faceBoxUrl, data.title, data.image)
                } else if ( data.type === ItemTypeEnum.TAG) {
                    await this.machineBoxApi.teachTag(data.root.tagBoxUrl, data.title, data.image)
                }
                data.status = ProcessStatusEnum.SUCCESS;
            } catch ( err ) {
                data.status = ProcessStatusEnum.FAILED;
                data.failedMessage = err.message;
            }

            await this.itemRepo.save(data)
        }

        if ( await this.getWorkerCount() === 0 ) {
            this.start(data.rootId);
        }

        done()

    }

    /**
     * 佇列啟動程式 (預設每次取20筆工作加入佇列)
     */
    async start(rootId: number) {
        let items = await this.itemRepo.find({
            where: {
                rootId,
                status: ProcessStatusEnum.PROCESSING
            },
            take: 20
        })

        if ( items.length > 0 ) {
            this.Queue.addBulk(items.map(x => ({
                data: x.id
            })))
        } else {
            await this.rootRepo.update({
                id: rootId,
                status: ProcessStatusEnum.PROCESSING
            }, {
                status: ProcessStatusEnum.SUCCESS,
                doneAt: new Date()
            })

            const nextRoot = await this.rootRepo.findOne({
                where: {
                    id: MoreThan(rootId),
                    status: ProcessStatusEnum.PROCESSING
                }
            })

            if ( nextRoot !== undefined ) {
                this.start(nextRoot.id)
            }
        }
    }
}