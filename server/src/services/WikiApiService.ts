import { BaseService } from "./BaseService";
import { WikiApiParseBody } from "../models/WikiApi/WikiParseBody";
import { WikiApiImageQueryBody } from "../models/WikiApi//WikiImageQueryBody";
import { WikiApiErrorBody, hasError } from '../models/WikiApi/WikiCrawlerErrorBody';
import { WikiApiQueryPageImageBody } from '../models/WikiApi/WikiQueryBody';
import axios from 'axios'
import { Service } from "@tsed/di";

@Service()
export class WikiApiService extends BaseService {
    wikiApiUrl = 'https://zh.wikipedia.org/w/api.php'
    
    getWikiTitle(url: string) { 
        return decodeURI(url.split('/').pop())
    }   

    /**
     * 取得圖片實體網址
     * @param title 
     */
    private async getImageUrl(title: string): Promise<string> {
        const res = await axios.request<WikiApiImageQueryBody>({
            url: this.wikiApiUrl,
            params: {
                action: 'query',
                format: 'json',
                origin: '*',
                titles: `File:${title}`,
                prop: 'imageinfo',
                iiprop: 'url|size|mime'
            }
        })

        try {
            if (res.data.query !== undefined) {
                const [value] = Object.values(res.data.query.pages)
                const [imageinfo] = value.imageinfo
                return decodeURI(imageinfo.url)
            }
        } catch (err) {
            return undefined
        }
    }

    /**
     * 取得維基相關資料
     * @param title 
     */
    async getWikiPage(title: string): Promise<{
        title: string,
        links: string[],
        images: string[],
        categorys: string[],
        isPeople: boolean,
        pageImage: string
    } | undefined> {
        const pageInfoRes = await axios.request<WikiApiParseBody | WikiApiErrorBody>({
            url: this.wikiApiUrl,
            params: {
                action: 'parse',
                format: 'json',
                origin: '*',
                page: title,
                prop: 'images|links|categories'
            }
        })

        if (hasError(pageInfoRes.data)) {
            return undefined
        }

        const pageImageRes = await axios.request<WikiApiQueryPageImageBody>({
            url: this.wikiApiUrl,
            params: {
                action: 'query',
                titles: title,
                format: 'json',
                origin: '*',
                prop: 'pageimages'
            }
        })

        const [ pageImage ] = Object.values(pageImageRes.data.query.pages)

        if (pageInfoRes.data.parse !== undefined ) {
            let imageUrl = ''
            if ( pageImage.pageimage !== undefined && pageImage.pageimage !== '' ) {
                imageUrl = await this.getImageUrl(pageImage.pageimage)
            }
            let lowerUrl = ''
            //machinebox 不支援svg
            if ( imageUrl !== undefined && imageUrl !== null ) {
                lowerUrl = imageUrl.toLowerCase()
                if ( lowerUrl.includes('.svg') ) {
                    imageUrl = undefined
                }
            }
            

            
            return {
                title: pageInfoRes.data.parse.title,
                pageImage: imageUrl,
                categorys: pageInfoRes.data.parse.categories.map(x => x['*']),
                links: pageInfoRes.data.parse.links === undefined ? [] : pageInfoRes.data.parse.links.map(x => x['*']),
                images: pageInfoRes.data.parse.images === undefined ? [] : pageInfoRes.data.parse.images,
                isPeople: pageInfoRes.data.parse.categories.some(x => x['*'].includes('年出生'))
            }
        }
    }

}