import {MigrationInterface, QueryRunner} from "typeorm";

export class fixImageType1608626868188 implements MigrationInterface {
    name = 'fixImageType1608626868188'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `teaching_item` CHANGE COLUMN `image` `image` LONGTEXT NULL DEFAULT NULL");
        await queryRunner.query("ALTER TABLE `wiki_item` CHANGE COLUMN `image` `image` LONGTEXT NULL DEFAULT NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `wiki_item` CHANGE COLUMN `image` `image` VARCHAR(255) NULL DEFAULT NULL ;");
        await queryRunner.query("ALTER TABLE `teaching_item` CHANGE COLUMN `image` `image` VARCHAR(255) NULL DEFAULT NULL ;");
    }

}
