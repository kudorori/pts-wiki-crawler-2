import {MigrationInterface, QueryRunner} from "typeorm";

export class init1607594912442 implements MigrationInterface {
    name = 'init1607594912442'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE `teaching_root` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime NULL, `updatedAt` datetime NULL, `deletedAt` datetime NULL, `fileName` varchar(255) NULL, `faceBoxUrl` varchar(255) NULL, `tagBoxUrl` varchar(255) NULL, `status` varchar(255) NULL, `failedMessage` text NULL, `doneAt` datetime NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `teaching_item` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime NULL, `updatedAt` datetime NULL, `deletedAt` datetime NULL, `rootId` int NULL, `image` varchar(255) NULL, `title` varchar(255) NULL, `type` varchar(255) NULL, `status` varchar(255) NULL, `failedMessage` varchar(255) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `wiki_root` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime NULL, `updatedAt` datetime NULL, `deletedAt` datetime NULL, `searchUrl` varchar(255) NULL, `depthNum` int NULL, `status` varchar(255) NULL, `failedMessage` text NULL, `doneAt` datetime NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `wiki_item` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime NULL, `updatedAt` datetime NULL, `deletedAt` datetime NULL, `rootId` int NULL, `parentId` int NULL, `title` varchar(255) NULL, `image` varchar(255) NULL, `searchUrl` varchar(255) NULL, `depthNum` int NULL, `type` varchar(255) NULL, `status` varchar(255) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `teaching_item` ADD CONSTRAINT `FK_1ce1a754fb93e9427712f681e43` FOREIGN KEY (`rootId`) REFERENCES `teaching_root`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `wiki_item` ADD CONSTRAINT `FK_a5248281b738c092f7c91902a87` FOREIGN KEY (`rootId`) REFERENCES `wiki_root`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `wiki_item` ADD CONSTRAINT `FK_a594becbf6adafe8181970ea505` FOREIGN KEY (`parentId`) REFERENCES `wiki_item`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `wiki_item` DROP FOREIGN KEY `FK_a594becbf6adafe8181970ea505`");
        await queryRunner.query("ALTER TABLE `wiki_item` DROP FOREIGN KEY `FK_a5248281b738c092f7c91902a87`");
        await queryRunner.query("ALTER TABLE `teaching_item` DROP FOREIGN KEY `FK_1ce1a754fb93e9427712f681e43`");
        await queryRunner.query("DROP TABLE `wiki_item`");
        await queryRunner.query("DROP TABLE `wiki_root`");
        await queryRunner.query("DROP TABLE `teaching_item`");
        await queryRunner.query("DROP TABLE `teaching_root`");
    }

}
