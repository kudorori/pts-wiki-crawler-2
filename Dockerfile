FROM node:12
EXPOSE 30010
WORKDIR /usr/src/server
COPY ./server .
COPY ./client /usr/src/client
RUN npm install
RUN npm run build
#初始化資料庫
CMD ["npm", "run", "docker:run"]