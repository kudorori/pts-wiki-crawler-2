# 維基爬蟲程式 

## 資料夾結構
```
|- server // API Server Dir
|- client // Static Web Dir
|- docker-compose.yaml // Docker Compose Script
|- Dockerfile  // Docker File Build
```
## 佈暑方式

> 本系統提供2種方式進行佈暑

### Docker

> Docker 的佈署設定可以參考 同目錄下的 `docker-compose.yaml` 以及 `Dockerfile`

#### 環境需求

```
    $ Docker version 19.03.13, build 4484c46d9d
    
    $ docker-compose version 1.27.4, build 40524192
```

#### 執行方式

```
    $ docker-compose up
```

### Nodejs

> 若是本地環境允許的話也可以單獨使用nodejs來執行，但是mysql & redis 麻煩需先建置好


#### 環境需求

```
    $ node -v v12.18.4
    $ npm -v 6.14.6
```

#### 執行方式

```javascript
    $ cd server
    $ npm intall
    $ npm run typeorm migration:run //初始化資料庫
    $ npm run start:prod
```



### Environment

> 以下環境變數麻煩需再執行前設定好，否則系統將會報錯 (或是可以參考./server/.env.example)

* DB_HOST=`MYSQL 路徑`
* DB_PORT=`MYSQL PORT`
* DB_USER=`MYSQL 登入帳號`
* DB_PASS=`MYSQL 登入密碼`
* DB_DATABASE=`MYSQL 資料庫`
* PORT=`網站執行的PORT`
* REDIS_HOST=`REDIS HOST`
* REDIS_PORT=`REDIS PORT`
* REDIS_PASSWORD=`REDIS PASSWORD`



### Reference

* Docker Images
    * https://hub.docker.com/_/node
    * https://hub.docker.com/r/machinebox/tagbox
    * https://hub.docker.com/r/machinebox/facebox
    * https://hub.docker.com/_/redis
    * https://hub.docker.com/_/mysql